from django.contrib.auth.models import User, Group
from rest_framework import serializers
from flowerhub.models import Member, Login, DomainName, MailingList, Contact, Website, EmailAlias, MysqlDatabase, MysqlUser, Access, EmailMailbox, DomainZone, MailingListDomain, WebsiteLogin, WebsiteDomainName
from django.core.exceptions import ValidationError
from django.forms import widgets
from django.utils.translation import gettext as _
from flowerhub.authorizer import Authz
from flowerhub import shared 

class MemberSerializer(serializers.ModelSerializer):
    def validate(self, data):
        super().validate(data)
        if not Authz.is_superuser():
            raise ValidationError(_("Only super users can create memberships via the API."))

        instance = Member(**data)
        try:
            instance.full_clean()
        except ValidationError as e:
            raise serializers.ValidationError(e.args[0])
        return data

    class Meta:
        model = Member 
        fields = '__all__' 

class MemberResourceSerializer(serializers.ModelSerializer):
    """
    Shared serializer for all Member Resources.
    """

    # We have to override all fields in all serializers that are
    # foreign keys, otherwise the auto-generated api will create
    # a drop down listing all options which will unintentionally
    # expose data.
    member = serializers.PrimaryKeyRelatedField(
        queryset = Member.objects.all(),
        style={'base_template': 'input.html'}
    )

    def validate(self, data):
        """
        Ensure all member resources are restricted to usernames with access
        to the member_id of the resource they are manipulating.
        """
        super().validate(data)
        if Authz.has_member_access(data['member'].id):
            return data
        else:
            raise ValidationError(_("You cannot save a record with that member id"))

class LoginSerializer(MemberResourceSerializer):
    def validate(self, data):
        super().validate(data)
        instance = Login(**data)
        try:
            instance.full_clean()
        except ValidationError as e:
            raise serializers.ValidationError(e.args[0])
        return data

    class Meta:
        model = Login 
        exclude = [ "uid", "partition", "mail_store" ]
        extra_kwargs = {'password': {'write_only': True}}

class AccessSerializer(MemberResourceSerializer):
    # Override login to avoid exposing all domain zone.
    # See comment on MemberResourceSerializer. 
    login = serializers.PrimaryKeyRelatedField(
        queryset = Login.objects.all(),
        style={'base_template': 'input.html'}
    )
    def validate(self, data):
        super().validate(data)
        instance = Access(**data)
        try:
            instance.full_clean()
        except ValidationError as e:
            raise serializers.ValidationError(e.args[0])
        return data

    class Meta:
        model = Access 
        fields = '__all__'

class DomainNameSerializer(MemberResourceSerializer):
    # Override domain_zone to avoid exposing all domain zone.
    # See comment on MemberResourceSerializer. 
    domain_zone = serializers.PrimaryKeyRelatedField(
        queryset = DomainZone.objects.all(),
        style={'base_template': 'input.html'}
    )
    def validate(self, data):
        super().validate(data)
        instance = DomainName(**data)
        try:
            instance.full_clean()
        except ValidationError as e:
            raise serializers.ValidationError(e.args[0])
        return data

    class Meta:
        model = DomainName 
        fields = '__all__'

class DomainZoneSerializer(MemberResourceSerializer):
    def validate(self, data):
        super().validate(data)
        instance = DomainZone(**data)
        try:
            instance.full_clean()
        except ValidationError as e:
            raise serializers.ValidationError(e.args[0])
        return data

    class Meta:
        model = DomainZone
        fields = '__all__'

class MailingListDomainSerializer(MemberResourceSerializer):
    # Override domain_zone to avoid exposing all domain zones.
    # See comment on MemberResourceSerializer. 
    domain_zone = serializers.PrimaryKeyRelatedField(
        queryset = DomainZone.objects.all(),
        style={'base_template': 'input.html'}
    )
    def validate(self, data):
        super().validate(data)
        instance = MailingListDomain(**data)
        try:
            instance.full_clean()
        except ValidationError as e:
            raise serializers.ValidationError(e.args[0])
        return data

    class Meta:
        model = MailingListDomain
        fields = '__all__'

class EmailMailboxSerializer(MemberResourceSerializer):
    # Override login to avoid exposing all logins.
    # See comment on MemberResourceSerializer. 
    login = serializers.PrimaryKeyRelatedField(
        queryset = Login.objects.all(),
        style={'base_template': 'input.html'}
    )
    def validate(self, data):
        super().validate(data)
        instance = EmailMailbox(**data)
        try:
            instance.full_clean()
        except ValidationError as e:
            raise serializers.ValidationError(e.args[0])
        return data

    class Meta:
        model = EmailMailbox 
        fields = '__all__'

class EmailAliasSerializer(MemberResourceSerializer):
    def validate(self, data):
        super().validate(data)
        instance = EmailAlias(**data)
        try:
            instance.full_clean()
        except ValidationError as e:
            raise serializers.ValidationError(e.args[0])
        return data

    class Meta:
        model = EmailAlias
        fields = '__all__'

class MysqlDatabaseSerializer(MemberResourceSerializer):
    def validate(self, data):
        super().validate(data)
        instance = MysqlDatabase(**data)
        try:
            instance.full_clean()
        except ValidationError as e:
            raise serializers.ValidationError(e.args[0])
        return data

    class Meta:
        model = MysqlDatabase
        fields = '__all__'

class MysqlUserSerializer(MemberResourceSerializer):
    # Override mysql_database to avoid exposing all dbs.
    # See comment on MemberResourceSerializer. 
    mysql_database = serializers.PrimaryKeyRelatedField(
        queryset = MysqlDatabase.objects.all(),
        style={'base_template': 'input.html'}
    )
    def validate(self, data):
        super().validate(data)
        instance = MysqlUser(**data)
        try:
            instance.full_clean()
        except ValidationError as e:
            raise serializers.ValidationError(e.args[0])
        return data

    class Meta:
        model = MysqlUser
        fields = '__all__'

class WebsiteSerializer(MemberResourceSerializer):
    # The Website model has two many to many fields - one for logins and one
    # for domain names. To create a Website via the API, you have to first
    # create the login, then create the domain name, then create the website,
    # then finally create the WebsiteDomainName and WebsiteLogin that unifies them all.
    def validate(self, data):
        super().validate(data)
        instance = Website(**data)
        try:
            instance.full_clean()
        except ValidationError as e:
            raise serializers.ValidationError(e.args[0])

        # There are some limits you can't exceed unless you are a super user
        # operating via the ui.
        if data['php_processes'] > Website.USER_LIMIT_PHP_PROCESSES:
            raise serializers.ValidationError({'php_processes': _("Only super users can raise php processes that high.")})
        if data['max_memory'] > Website.USER_LIMIT_MAX_MEMORY:
            raise serializers.ValidationError({'max_memory': _("Only super users can raise max memory that high.")})

        return data

    class Meta:
        model = Website 
        exclude = [ "proxy_settings", "partition", "website_store" ]

class WebsiteLoginSerializer(serializers.ModelSerializer):
    # Override login to avoid exposing all logins.
    # See comment on MemberResourceSerializer. 
    login = serializers.PrimaryKeyRelatedField(
        queryset = Login.objects.all(),
        style={'base_template': 'input.html'}
    )
    # Override website to avoid exposing all websites.
    # See comment on MemberResourceSerializer. 
    website = serializers.PrimaryKeyRelatedField(
        queryset = Website.objects.all(),
        style={'base_template': 'input.html'}
    )
    def validate(self, data):
        super().validate(data)
        instance = WebsiteLogin(**data)
        try:
            instance.full_clean()
        except ValidationError as e:
            raise serializers.ValidationError(e.args[0])

        # You must have access to the website side of this relationship
        # to avoid allowing a user to assign a login to a website
        # they should not have access to.
        if not Authz.has_member_access(data['website'].member_id):
            raise ValidationError(_("You don't have access to that website."))

        return data

    class Meta:
        model = WebsiteLogin 
        fields = '__all__'

class WebsiteDomainNameSerializer(serializers.ModelSerializer):
    # Override domain name to avoid exposing all domain names.
    # See comment on MemberResourceSerializer. 
    domain_name = serializers.PrimaryKeyRelatedField(
        queryset = DomainName.objects.all(),
        style={'base_template': 'input.html'}
    )
    # Override website to avoid exposing all web sites.
    # See comment on MemberResourceSerializer. 
    website = serializers.PrimaryKeyRelatedField(
        queryset = Website.objects.all(),
        style={'base_template': 'input.html'}
    )
    def validate(self, data):
        super().validate(data)
        instance = WebsiteDomainName(**data)
        try:
            instance.full_clean()
        except ValidationError as e:
            raise serializers.ValidationError(e.args[0])

        # You must have access to the website and domain side of this relationship
        # to avoid allowing a user to assign a domain name to a web site they
        # should not have access to.
        if not Authz.has_member_access(data['website'].member_id):
            raise ValidationError(_("You don't have access to that web site."))
        if not Authz.has_member_access(data['domain_name'].member_id):
            raise ValidationError(_("You don't have access to that domain name."))
        return data

    class Meta:
        model = WebsiteDomainName 
        fields = '__all__'

