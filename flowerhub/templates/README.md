# Templates

The template hierarchy goes like this:

```

                         base
                       /      \
         base_logged_in          base_logged_out
          /       \              /     \
  base_resource  base_generic  login   logged_out
