from django.db import models
from django.utils.translation import gettext_lazy as _
from django.urls import reverse
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist, ValidationError
import re
from django.core.validators import validate_email
from django.db import transaction
from publicsuffixlist import PublicSuffixList
import ipaddress
from django.conf import settings
from flowerhub import utils
from flowerhub.authorizer import Authz 
from flowerhub import shared 
import tempfile
import subprocess
from django.utils import timezone
import textwrap
import requests
import json
import types
import crypt
"""

Define validation functions. 

Validation happens in several places. In each model definition, the field
definitions have a validators attribute which references the functions below.
These functions can only take the value and have to be evaluated in isolation
of context. These validators are automatically used regardless of how the
record is inserted into the database (UI or API).

In addition, some models implement clean_field methods. The clean_field methods
are nice because they are passed a self object, so you can validate fields in
relationship to other fields. Each API serialization class must call
clean_fields to ensure that validation happens via the API.
"""

def valid_username(value):
    """
    Ensure usernames only have lower case alpha numeric characters or dashes. For 
    historic reasons we allow the @ sign in a username. This also helps groups
    transitioning from hosts in which their email address was their username.
    """
    if not re.match('^[a-z0-9-_@.]+$', value):
        raise ValidationError(
            _('Usernames must be lower case and limited to letters, numbers, dashes, underscores, the @ sign and periods. %(value)s includes characters that are not allowed.'),
            params={'value': value},
        )

def valid_user_portion(value):
    """
    The validate_user_portion is used for the user portion of email addresses (and list names).
    """
    if not re.match('^[a-z0-9-_.]+$', value):
        raise ValidationError(
            _('User portion of an email address must be lower case and limited to letters, numbers, dashes, underscores, and periods. %(value)s includes characters that are not allowed.'),
            params={'value': value},
        )

def valid_domain_zone(value):
    """
    Ensure the domain zone does not contain invalid characters. Note: domain
    names are allowed to have underscores, but not zones. Also, domain names
    technically can have upper case letters but we enforce lower case for
    consistency.
    """
    # Basic sanity check.
    if not re.match('^[a-z0-9-.]+$', value):
        raise ValidationError(
            _("The domain zone you entered (%(value)s) has some invalid characters. NOTE: please use all lower case."),
            params={'value': value},
        )

    
# The domain label is the portion of the domain name that comes before
# the domain zone.
def valid_domain_label(value):
    # We allow wild card domains, but only one!
    if value == '*':
        return
    if value.count('*') > 1:
        raise ValidationError(
            _("A domain name label can only have one asterisk in it.")
        )

    label_parts = value.split('.')
    for part in label_parts:
        # Just like domain zones, but labels can have the _ in them.
        if not re.match('^[a-z0-9-_]+$', value):
            raise ValidationError(
                _("The domain label you entered (%(value)s) has some invalid characters. NOTE: please use all lower case."),
                    params={'value': value},
            )

# If given a fqdn, is it valid?
def valid_domain_name(value):
    # Make sure the domain zone is valid.
    psl = PublicSuffixList()
    private_suffix = psl.privatesuffix(value)
    valid_domain_zone(private_suffix)

    if private_suffix == value:
        # If we are passed the domain zone, we are done.
        return

    # Now make sure the rest is valid. The domain label test is more loose
    # then the domain zone test, so pass the whole thing through the domain
    # label test and we should be good
    valid_domain_label(value)

def valid_domain_ttl(value):
    if int(value) < 300:
        raise ValidationError(
            _("Please use a value greater than or equal to 300. Lower values cannot be guaranteed.")
        )

def valid_domain_server_name(value):
    if not re.match('^[a-z0-9-._]+$', value):
        raise ValidationError(
            _("The server name you entered (%(value)s) has some invalid characters. NOTE: please use all lower case."),
                params={'value': value},
        )
    if not value.find('.'):
        raise ValidationError(
            _("The server name should be a valid domain with at least one period in it.")
        )

def valid_space_separated_email_addresses(value):
    if ',' in value:
        raise ValidationError(
            _("Please use a space to separate email addresses, not a comma.")
        )
    if ';' in value:
        raise ValidationError(
            _("Please use a space to separate email addresses, not a semi-colon.")
        )

    for email in value.split(' '):
        validate_email(email)

def valid_password(value):
    # You can leave the password blank when not updating the password. Only
    # validate if a value is provided.
    if value:
        if len(value) < settings.FLOWER_PASSWORD_MINIMUM_LENGTH:
            raise ValidationError(
                _("Please enter a password that is at least %(length)s characters long. Try putting two or three different words together to make your password."), params={'length': settings.FLOWER_PASSWORD_MINIMUM_LENGTH},
            )
        # If a word dictionary is available, ensure the password is not in it.
        dictionary = '/usr/share/dict/words'
        with open(dictionary, 'r') as f:
            for line in f:
                if value == line.rstrip():
                    raise ValidationError(
                        _("Your password is an exact match for a word in the English dictionary. Please choose a word that does not appear in the dictionary. Try putting 2 or 3 dictionary words together instead.")
                    )

def valid_ssh_public_key_file(value):
    # First make sure we aren't getting bombed with a huge string.
    # A typically public key is about 500 characters. Let's allow for
    # a max of about 20 keys, or 10,000 characters.
    if len(value) > 10000:
        raise ValidationError(
            _("You can only submit 10,000 characters in this field.")
        )

    with tempfile.NamedTemporaryFile(mode='w+', encoding='utf-8') as fp:
        fp.write(value)
        # If we don't reset the file pointer, ssh-keygen doesn't work.
        fp.seek(0)
        cmd = [ "/usr/bin/ssh-keygen", "-l", "-f", fp.name ]
        result = subprocess.run(cmd)

        # Close should delete the file.
        fp.close()

        if result.returncode != 0:
            raise ValidationError(
                _("The ssh public key you entered was not recognized by the ssh-keygen command. Please check it and try again.")
            )

def valid_mysql_database(value):
    # Can only contain letters, numbers and underscore, must start with letter.
    if not re.match('^([a-z][a-z_0-9]*){1,64}$', value):
        raise ValidationError(
            _("That database name is not valid. Please use only lower case letters, numbers and underscores. The name must start with a letter and be limited to 64 characters.")
        )

def valid_mysql_user(value):
    # Can only contain letters, numbers and underscore, must start with letter.
    if not re.match('^([a-z][a-z_0-9]*){1,16}$', value):
        raise ValidationError(
            _("That database user name is not valid. Please use only lower case letters, numbers and underscores. The name must start with a letter and be limited to 16 characters.")
        )

def valid_path(value):
    if not re.match('^[a-zA-Z0-9-_./]{1,255}$', value):
        raise ValidationError(
            _("Your path contains characters that are not allowed or is more than 255 characters long.")
        )
    # Don't allow double dot sneakiness.
    if '..' in value:
        raise ValidationError(
            _("Please don't include .. in your path, you sneaky bastard.")
        )
    if value.startswith('/'):
        raise ValidationError(
            _("Please don't start the website root with a slash.")
        )

def valid_website_settings(value):
    for line in value.splitlines():
        line = line.strip()
        if line == "":
            # Blank lines are just fine.
            continue
        words = line.split()
        if words[0] == "#":
            # Comments are also safe
            continue

        # Otherwise, we have to check the first word to make sure it matches
        # something acceptable.
        if not re.match('^(ServerAdmin|Alias|ScriptAlias|ScriptAliasMatch|DirectoryIndex|RedirectCond|RedirectMatch|RedirectEngine|RewriteEngine|RewriteCond|RewriteRule|RewriteMap|Options|AllowOverride|Order|Allow|Deny|<Directory|</Directory>|<Files|</Files>|<FilesMatch|</FilesMatch>|RewriteBase|Redirect|AuthUserFile|AuthName|AuthType|require|Require|<Location|</Location>|<LocationMatch|</LocationMatch>|SetEnv|AddType|SuexecUserGroup|LogLevel|Action|AddHandler|Satisfy|ErrorDocument|PerlAccessHandler|SetHandler|PerlHandler|DAV|ProxyErrorOverride|ProxyPass|SSLVerifyClient|SSLVerifyDepth|SSLCACertificateFile|<If|</If|ProxyHeader)$', words[0]):
            raise ValidationError(
                _("Your website settins contain a line that is not allowed. Only the following values are allowed in website configuration settings: ServerAdmin, Alias, ScriptAlias, ScriptAliasMatch, RedirectCond, RedirectMatch, RedirectEngine, RewriteCond, RewriteRule, RewriteEngine, RewriteMap, AllowOverride, Order, Deny, Options, Allow, Directory (both opening and closing), Files and FilesMatch (both opening and closing), RewriteBase, DirectoryIndex, Redirect, AuthUserFile, AuthName, AuthType, require, Location (both opening and closing), SetEnv, AddType, SuexecUserGroup, ErrorLog, LogLevel, Action, Satisfy, AddHandler, SetHandler, PerlHandler, PerlAccessHandler, ErrorDocument, DAV, SSLVerifyClient, SSLVerifyDepth, SSLCACertificateFile, If, and ProxyHeader. You entered %(key)s") % ( {'key': words[0]})
            )

def valid_rank(value):
    # 0 is unknown and we want this field to be required.
    if value < 1:
        raise ValidationError(
            _("Please select a ranking from the provided options.")
        )
        
def valid_sshfp_algorithm(value):
    if value < 0 or value > 4:
        raise ValidationError(
            _("A valid sshfp algorithm is between 1 and 4 (or zero if not an sshfp record).")
        )

def valid_sshfp_type(value):
    if value < 0 or value > 2:
        raise ValidationError(
            _("A valid sshfp type is between 1 and 2 (or zero if not an sshfp record).")
        )

def member_under_quota(member_id, added_quota, exclude_id = None, exclude_model = None):
    """
    This validation routine checks to see if the record that is about to be added
    will result in the member being over the disk quota. 

    added_quota: the total quota amount the user is submitting via the given request
    exclude_model: the model being submitted
    exclude_id: If the record being submitted already exists in the database, provide the id
      so we can exclude whatever totals are already in the database.

    Return True if we 
    """

    mailbox_total = 0
    nextcloud_total = 0
    website_total = 0

    member = Member.objects.get(id=member_id)
    member_total = member.disk_quota

    # We have three resources to check: Login.mailbox_quota, Login.nextcloud_quota and Website.disk_quota.
    if exclude_id and exclude_model == 'Login':
        result = Login.objects.filter(member=member_id).exclude(id=exclude_id).aggregate(models.Sum('mailbox_quota'))
    else:
        result = Login.objects.filter(member=member_id).aggregate(models.Sum('mailbox_quota'))

    if result:
        mailbox_total = result['mailbox_quota__sum'] or 0

    if exclude_id and exclude_model == 'Login':
        result = Login.objects.filter(member=member_id).exclude(id=exclude_id).aggregate(models.Sum('nextcloud_quota'))
    else:
        result = Login.objects.filter(member=member_id).aggregate(models.Sum('nextcloud_quota'))

    if result:
        nextcloud_total = result['nextcloud_quota__sum'] or 0

    if exclude_id and exclude_model == 'Website':
        result = Website.objects.filter(member=member_id).exclude(id=exclude_id).aggregate(models.Sum('disk_quota'))
    else:
        result = Website.objects.filter(member=member_id).aggregate(models.Sum('disk_quota'))

    if result:
        website_total = result['disk_quota__sum'] or 0

    return member_total > (mailbox_total + nextcloud_total + website_total + added_quota)

# Helper function - return all classes that are Member resource classes.
def get_all_member_resource_classes(enable_order=True):

    # Some resources depend on other resources to be properly created. By default, return them
    # in the order in which they should be enabled. If enable_order=False, reverse it.
    resources = [  Login, DomainZone, DomainName, MailingListDomain, MailingList, Website, EmailMailbox, EmailAlias, MysqlDatabase, MysqlUser, Access,  DomainZone,  Access, Contact, ]

    if not enable_order:
        resources.reverse()

    return resources

# Define our abstract base classes first.
class MemberResource(models.Model):
    """
    All our main "things" are referred to as member resources and share this base class.
    """

    # Each resource has one or more petal records representing each petal 
    # it is installed on. Each petal record has their own means of tracking the
    # status on that particular server. The resource itself uses the following
    # constants to summarize it's status.
    STATUS_EXECUTE_ERROR = 1 
    STATUS_QUEUED = 2 
    STATUS_INPROGRESS = 3 
    STATUS_DISABLED = 4 
    STATUS_ACTIVE = 5 

    # All member resources have an is_active field that determines whether or
    # not the resource is disabled or not. Unfortunately, it's not that simple
    # though. A resource is either active (1, which can be translatd as
    # "True") or disabled manually (0) or disabled automatically when a
    # membership is disabled (-1). This last bit is important - if a membership
    # is disabled and then re-enabled, we have to only re-enable the resoruces
    # that were automatically disabled, and leave the ones manually disabled
    # off.
    IS_ACTIVE = 1
    IS_DISABLED_BY_USER = 0
    IS_DISABLED_AUTO = -1

    IS_ACTIVE_CHOICES = (
        (IS_ACTIVE, _("Active")),
        (IS_DISABLED_BY_USER, _("User disabled")),
        (IS_DISABLED_AUTO, _("Auto disabled"))
    )

    is_active = models.SmallIntegerField(choices=IS_ACTIVE_CHOICES, default=IS_ACTIVE, db_index=True)

    # Helper function used in the templates to determine whether or not
    # a resource should be greyed out or not.
    def display_as_disabled(self):
        if self.is_active == self.IS_ACTIVE:
            return False
        return True


    """
    Return a single status for the resource. 
    
    This is tricky, because one resource could have more than one petal, which
    means it could have a success on one petal but a failure on the other. This
    response has to encompass all the petals by displaying the slowest or most
    troublesome petal response.  

    """
    status = None

    def get_status(self):
        # Establish base line default status. We're either active or disabled
        # to begin with.
        if self.is_active == self.IS_ACTIVE: 
            status = self.STATUS_ACTIVE 
        else:
            status = self.STATUS_DISABLED

        for runner in self.get_active_runners():
            # First check for errors. If just one petal has an error, we report
            # this as an error.
            if runner.error:
                # This is the worst. A petal reported that it could not complete
                # the request. We may as well return early. Nothing trumps execute
                # error.
                return self.STATUS_EXECUTE_ERROR 
            
            # At least one runner has not even been submitted to the petal. This
            # is the lowest status, so we will always take this status as the over
            # all resource status.
            if runner.current_status == RunnerCommon.STATUS_PENDING_QUEUED:
                status = self.STATUS_QUEUED 

            # At least one runner has been received by the petal, but it has not
            # completed (or it stalled). As long as no other runner is in a lower
            # state, we will use this status.
            if runner.current_status == RunnerCommon.STATUS_PENDING_INPROGRESS:
                if status > self.STATUS_QUEUED:
                    status = self.STATUS_INPROGRESS 

        self.status = status
        return self.status


    """ 
    Get displayable data for the status of this resource. 
        
    We return a tuple with both a string html class (which will display a font
    awseome icon) and a string description.

    """
    def get_display_status(self):
        RESPONSES = {
            self.STATUS_EXECUTE_ERROR: _("Error"),
            self.STATUS_QUEUED: _("Queued"),
            self.STATUS_INPROGRESS: _("In Progress"),
            self.STATUS_DISABLED: _("Disabled"),
            self.STATUS_ACTIVE: _("Active"),
        }       
        response = self.get_status()
        return RESPONSES[response]
    
    # You can't edit resources with changes pending.
    def is_editable(self):
        status = self.get_status()
        if status == self.STATUS_QUEUED or status == self.STATUS_INPROGRESS:
            return False
        return True

    def clean(self):
        super().clean()
        if not self.is_editable():
            raise ValidationError(
                _("You cannot edit a record that is pending a change.")
            )

    """
    The methods below are relate to the creation and management of runners. Most 
    resources create something on a petal servers. Some create different things on
    different petal servers. All resources that create something on a petal server
    create a new runner record that keeps track of the status of the resource state
    change for that given server.
    """
    # If you create runners, return the model class that can save them to the database. 
    def get_runner_object(self=None):
        return None

    # Helper function. Don't call directly, use get_active_runners or get_all_runners
    # instead.
    def get_runners(self, is_active=True):
        ro = self.get_runner_object()
        if not ro:
            return []

        # If we have not yet been saved, we have no runners.
        if not self.pk:
            return []

        if is_active:
            # Restrict to just active runners.
            return ro.objects.filter(resource=self, is_active=True).order_by('-nonce_created')

        return ro.objects.filter(resource=self).order_by('-nonce_created')

    # Runners are active if they are pending OR they are the most recent active record.
    def get_active_runners(self):
        return self.get_runners()

    # The list of all runners provides a history of change for the resource.
    def get_all_runners(self):
        is_active = False
        return self.get_runners(is_active)

    def get_default_petals(self, category):
        return Petal.objects.filter(category=category, is_default=True)

    # For a given Petal category, return a list of either all petals that this
    # resource is currently active on OR a list of the petals it should be
    # created on (if it is not yet created).
    def get_petals_for_category(self, category):
        petals = []
        # If we have existing active runners, create new runners on these petals
        for active_runner in self.get_active_runners():
            if active_runner.petal.category == category:
                petals.append(active_runner.petal)

        # If we don't have any active runners, create new ones using the default
        # petals.
        if not petals:
            petals = self.get_default_petals(category)

        return petals

    # Every time we change a resource, we create new runners AND we have to
    # retire the old active runners. This method retires the old active ones.
    def retire_active_runners(self, category):
        for active_runner in self.get_active_runners():
            if active_runner.petal.category == category:
                active_runner.is_active = False
                active_runner.save()

    # Every member resource that maintains runners should implement this method
    # to retire existing runners and create new ones with the right data.
    def setup_runners(self, desired_status=None):
        pass

    # This helper function takes a list of petals, desired status and data
    # and creates and submits new runners.
    def create_runners(self, petals=[], petal_action=None, desired_status=None, data={}):
        for petal in petals:
            ro = self.get_runner_object()
            r = ro(nonce=utils.get_random_password(64), petal=petal, resource=self, petal_action=petal_action, desired_status=desired_status, data=json.dumps(data))
            r.save() 
            # Now post this data to the petal itself so the resource can be created.
            r.submit()

    def delete(self, using=None, keep_parents=False):
        # Create the related runners before we delete (otherwise we get a django error).
        self.setup_runners(desired_status=RunnerCommon.STATUS_DELETED)

        # Now, delete from the database. Adios.
        super().delete(using, keep_parents)

    def save(self, *args, **kwargs):
        # First properly save the record.
        super().save(*args, **kwargs)

        if self.is_active == self.IS_ACTIVE:
            desired_status = RunnerCommon.STATUS_ACTIVE
        else:
            desired_status = RunnerCommon.STATUS_DISABLED

        self.setup_runners(desired_status=desired_status)
        
    class Meta:
        abstract = True

class RunnerCommon(models.Model):
    """
    Some resources have one or more related petal records. The are called
    "runners."

    A resource has a related petal record if it requires some action to be taken
    on a host in order for it to be completed. A resource might have more than
    one runner (e.g. both a webstore runner record to create the dierctory
    on the server hosting the web site and also a websitecache record to ensure the
    cache is setup on the website caching server).

    All runners track the status of the setup of the resource on each petal,
    including the status it is currently in and (if the current status is one
    of the pending states), the status we are trying to be in. It also stores a
    text representation of the resource itself, as well as a nonce to track
    communication with the petal.

    """

    # Status choices
    STATUS_PENDING_QUEUED = 1
    STATUS_PENDING_INPROGRESS = 2
    STATUS_ACTIVE = 3
    STATUS_DISABLED = 4
    STATUS_DELETED = 5

    CURRENT_STATUS_CHOICES = (
        (STATUS_PENDING_QUEUED, _('queued')),
        (STATUS_PENDING_INPROGRESS, _('in progress')),
        (STATUS_ACTIVE, _('active')),
        (STATUS_DISABLED, _('disabled')),
        (STATUS_DELETED, _('deleted')),
    )
    DESIRED_STATUS_CHOICES = (
        (STATUS_ACTIVE, _('active')),
        (STATUS_DISABLED, _('disabled')),
        (STATUS_DELETED, _('deleted')),
    )

    PETAL_ACTION_NOOP = shared.PETAL_ACTION_NOOP
    PETAL_ACTION_CREATE_LDAP_LOGIN = shared.PETAL_ACTION_CREATE_LDAP_LOGIN
    PETAL_ACTION_DELETE_LDAP_LOGIN = shared.PETAL_ACTION_DELETE_LDAP_LOGIN
    PETAL_ACTION_DISABLE_LDAP_LOGIN = shared.PETAL_ACTION_DISABLE_LDAP_LOGIN
    PETAL_ACTION_ADD_EMAIL_ADDRESS = shared.PETAL_ACTION_ADD_EMAIL_ADDRESS
    PETAL_ACTION_REMOVE_EMAIL_ADDRESS = shared.PETAL_ACTION_REMOVE_EMAIL_ADDRESS
    PETAL_ACTION_REMOVE_LAST_EMAIL_ADDRESS = shared.PETAL_ACTION_REMOVE_LAST_EMAIL_ADDRESS
    PETAL_ACTION_CREATE_DOMAIN_ZONE = shared.PETAL_ACTION_CREATE_DOMAIN_ZONE
    PETAL_ACTION_DELETE_DOMAIN_ZONE = shared.PETAL_ACTION_DELETE_DOMAIN_ZONE
    PETAL_ACTION_CREATE_MAILDIR = shared.PETAL_ACTION_CREATE_MAILDIR
    PETAL_ACTION_DELETE_MAILDIR = shared.PETAL_ACTION_DELETE_MAILDIR
    PETAL_ACTION_ADD_EMAIL_DOMAIN = shared.PETAL_ACTION_ADD_EMAIL_DOMAIN
    PETAL_ACTION_REMOVE_EMAIL_DOMAIN = shared.PETAL_ACTION_REMOVE_EMAIL_DOMAIN
    PETAL_ACTION_ADD_EMAIL_ALIAS = shared.PETAL_ACTION_ADD_EMAIL_ALIAS
    PETAL_ACTION_REMOVE_EMAIL_ALIAS = shared.PETAL_ACTION_REMOVE_EMAIL_ALIAS
    PETAL_ACTION_CREATE_WEBSTORE_SITE = shared.PETAL_ACTION_CREATE_WEBSTORE_SITE    
    PETAL_ACTION_DISABLE_WEBSTORE_SITE = shared.PETAL_ACTION_DISABLE_WEBSTORE_SITE 
    PETAL_ACTION_DELETE_WEBSTORE_SITE = shared.PETAL_ACTION_DELETE_WEBSTORE_SITE 

    PETAL_ACTION_CHOICES = (
        (PETAL_ACTION_NOOP, _("No operation required.")),
        (PETAL_ACTION_CREATE_LDAP_LOGIN, _("Create or update LDAP login")),
        (PETAL_ACTION_DELETE_LDAP_LOGIN, _("Delete LDAP login")),
        (PETAL_ACTION_DISABLE_LDAP_LOGIN, _("Disable LDAP login")),
        (PETAL_ACTION_ADD_EMAIL_ADDRESS, _("Add email address to LDAP record")),
        (PETAL_ACTION_REMOVE_EMAIL_ADDRESS, _("Remove or disable email address from LDAP record.")),
        (PETAL_ACTION_REMOVE_LAST_EMAIL_ADDRESS, _("Remove or disable last email address from LDAP record.")),
        (PETAL_ACTION_CREATE_DOMAIN_ZONE, _("Create or update a domain zone.")),
        (PETAL_ACTION_DELETE_DOMAIN_ZONE, _("Delete a domain zone.")),
        (PETAL_ACTION_CREATE_MAILDIR, _("Ensure Maildir exists.")),
        (PETAL_ACTION_DELETE_MAILDIR, _("Delete Maildir.")),
        (PETAL_ACTION_ADD_EMAIL_DOMAIN, _("Add email domain.")),
        (PETAL_ACTION_REMOVE_EMAIL_DOMAIN, _("Remove email domain.")),
        (PETAL_ACTION_ADD_EMAIL_ALIAS, _("Add email alias.")),
        (PETAL_ACTION_REMOVE_EMAIL_ALIAS, _("Remove email alias.")),
        (PETAL_ACTION_CREATE_WEBSTORE_SITE, _("Create website on origin server.")),
        (PETAL_ACTION_DISABLE_WEBSTORE_SITE, _("Disable website on origin server.")),
        (PETAL_ACTION_DELETE_WEBSTORE_SITE, _("Delete website on origin server.")),
    )

    current_status = models.SmallIntegerField(choices=CURRENT_STATUS_CHOICES, default=STATUS_PENDING_QUEUED, db_index=True)
    desired_status = models.SmallIntegerField(choices=DESIRED_STATUS_CHOICES, default=STATUS_ACTIVE)
    nonce = models.CharField(max_length=64, help_text=_("A random string used to authenticate the petal response after attempting to create/edit/delete the resource on the petal."))
    nonce_created = models.DateTimeField(auto_now_add=True, help_text=_("The date/time the nonce was created."))
    nonce_consumed = models.DateTimeField(blank=True, null=True,help_text=_("The date/time the nonce was used."))
    data = models.TextField(help_text=_("A text representation of the resource fields and values to be created."))
    petal_action = models.SmallIntegerField(choices=PETAL_ACTION_CHOICES, help_text=_("The class responsible for implementing this action on the petal."))
    error = models.CharField(blank=True, max_length=254, help_text=_("Any errors encountered when creating the resource."))
    is_active = models.BooleanField(default=True, help_text=_("When a resource is re-edited, all previous runners are retired so we always know which ones are active."))
    modified_by = models.CharField(max_length=64, blank=True, help_text=_("String username of the user who modified this record."))

    def save(self, *args, **kwargs):
        # If we are in DEMO, then we are not configured to submit this petal
        # request, we update the status on the fly so the status is always
        # active (makes demo work more nicely)
        if settings.FLOWER_DEMO:
            self.current_status = self.STATUS_ACTIVE

        # When re-submitting requests that did not go through, we don't want to override
        # the username of the original submitter.
        if not self.modified_by:
            username = Authz.get_username()
            if settings.FLOWER_DEMO and username == None:
                # In demo mode, the initial resource items are created when the user
                # is still anonymous.
                username = 'anonymous'
            self.modified_by = username 
        super().save(*args, **kwargs)

    # Submit ourselves to the petal so this resource can be created.
    def submit(self):
        if not settings.FLOWER_DEMO:
            # We have to determine our resource class name. 
            if self.resource:
                # Piece of cake:
                resource_class = self.resource.__class__.__name__
            else:
                # That won't work if resource has already been deleted.
                # So, we should use our class name and remove "Runner"
                resource_class = str(self.__class__.__name__).replace("Runner", "")

            post = {
                'nonce': self.nonce,
                'data': self.data,
                'api_key': settings.FLOWER_PETAL_API_KEY,
                'resource_class': resource_class,
                'petal_action': self.petal_action,
            }
            try:
                shared.log_info("Submitting to {0}, with action {1}, with nonce {2} expiring on (more or less) {3}.".format(self.petal.url, self.get_petal_action_display(), self.nonce, timezone.now()))
                r = requests.post(self.petal.url, data=post, verify="/etc/ssl/certs/ca-certificates.crt")
            except requests.exceptions.ConnectionError as e:
                # Treat connection error like an invented 600 status code.
                r = types.SimpleNamespace()
                r.status_code = 600
                r.text = str(e)

            if r.status_code != 200:
                shared.log_error("Failed to reach url {0}, got return code {1} and text {2}".format(self.petal.url, r.status_code, r.text[0:200]))
                # It would be nice to notify the user, but we are in the model.py
                # so we can't. We could try to refactor this code to execute in
                # views.py if we wanted to interact with the user, but then it 
                # wouldn't trigger on an API call. Sigh. The user is informed via
                # the change in status, which is subtle but at least something.
                #
                # Continue to the next runner in the list.
                return
            else:
                # Update - resource is received.
                self.current_status = self.STATUS_PENDING_INPROGRESS
        else:
            # In demo mode we make everything active immediately since we don't submit
            # to anything.
            self.current_status = self.STATUS_ACTIVE

        self.save()

    def __str__(self):
        return str(self.resource) + str(self.petal)

    class Meta:
        abstract = True

# Now, the actual models.
class Feedback(models.Model):

    RANK_UNKNOWN = 0
    RANK_GOOD = 1
    RANK_MEDIUM = 2
    RANK_BAD = 3

    RANK_CHOICES = (
        (RANK_UNKNOWN, _("Choose one")),
        (RANK_GOOD, _("Good")),
        (RANK_MEDIUM, _("Medium")),
        (RANK_BAD, _("Bad")),
    )

    rank = models.SmallIntegerField(help_text=_("What is your overall impression? Required - please choose one."), choices=RANK_CHOICES, default=RANK_UNKNOWN, validators=[valid_rank])
    comments = models.TextField(blank=True, help_text=_("Optionally, Please let us know what you think and if you have any suggestions for improvements."))

    def __str__(self):
        return self.id

    def get_absolute_url(self):
        return reverse('feedback-detail', args=[self.id])

class Petal(models.Model):
    """
    The petal table contains a row for every server.

    Each petal has a hostname (which is the DNS-accessible address of the host
    and is considered private and may be randomly generated) and a label that
    is human-readable and publicly accessible to our members so they can easily
    identify which server they are on and more intelligently read our service
    advisories, which will reference the label rather than the hostname.
    """

    LDAP = 1 
    NSAUTH = 2 
    NSCACHE = 3 
    SHELL = 4 
    WEBSTORE = 5 
    WEBPROXY = 6 
    MAILSTORE = 7 
    MAILCF = 8 
    MAILMX = 9 
    MAILRELAY = 10 
    TEST = 11 
  
    CATEGORY_CHOICES = (
        (LDAP, 'ldap'),
        (NSAUTH, 'nsauth'),
        (NSCACHE, 'nscache'),
        (SHELL, 'shell'),
        (WEBSTORE, 'webstore'),
        (WEBPROXY, 'webproxy'),
        (MAILSTORE, 'mailstore'),
        (MAILCF, 'mailcf'),
        (MAILMX, 'mailmx'),
        (MAILRELAY, 'mailrelay'),
        (TEST, 'test'),
    )
  
    url = models.CharField(max_length=254, help_text=_("The full URL, including schema and port, that will listen for new requests sent by the flower hub."))
    hostname = models.CharField(max_length=64, unique=True, help_text=_("The fully qualified domain name of the host that will be used by proxy servers to locate the petal."))
    category = models.SmallIntegerField(choices=CATEGORY_CHOICES, db_index=True)
    is_default = models.BooleanField(db_index=True, default=False)

    def clean(self):
        super().clean()

        # If this petal is not the default, ensure that the category we are
        # using still has a default.
        if not self.is_default:
            if not Petal.objects.filter(is_default=True, category=self.category).exists():
                raise ValidationError(
                    _("Please ensure that at least one petal of your type remains as the default.")
                )

    def save(self, *args, **kwargs):
        # If this petal is set to is_default, then uncheck any existing is_default petals of the same category.
        # First save this one
        super().save(*args, **kwargs)

        # Next, hunt for others.
        if self.is_default:
            Petal.objects.filter(is_default=True, category=self.category).exclude(pk=self.pk).update(is_default=False)

    def __str__(self):
        return self.hostname

    def get_absolute_url(self):
        return reverse('petal-update', args=[self.id])

class Partition(models.Model):
    """
    The partition table contains a row for every data partition.

    Each data partition is assigned to one petal server. A petal server
    can have zero or more partitions, but only one partition that is
    marked as the default for resources created on it.

    The partitions are only relevant for mailstore and webstore petal servers.
    Each website or mailbox is stored on a given partition. Different
    websites and mailboxes on the same server can be stored on different
    partitions.
    """

    petal = models.ForeignKey(Petal, on_delete=models.SET_NULL, blank=True, null=True, limit_choices_to=models.Q(category=Petal.WEBSTORE) | models.Q(category=Petal.MAILSTORE))
    is_default = models.BooleanField(db_index=True, default=False)

    def clean(self):
        super().clean()

        # If this partition is not the default, ensure that the petal we are
        # saving against still has a default. (It is possible to save a
        # partition without a petal, in which case is_default is moot.)
        if self.petal:
            if not self.is_default:
                if not Partition.objects.filter(is_default=True, petal=self.petal).exists():
                    raise ValidationError(
                        _("Please ensure that at least one partition remains as the default for this petal.")
                    )
    def save(self, *args, **kwargs):
        # If this partition is set to is_default, then uncheck any existing is_default partitions.
        # We should only have one.
        # First save this one
        super().save(*args, **kwargs)

        # Next, hunt for others.
        if self.petal:
            if self.is_default:
                Partition.objects.filter(is_default=True, petal=self.petal).exclude(pk=self.pk).update(is_default=False)


    # When mounting this partition, we will call it data0001, data0002, etc depending
    # on the primary key.
    def get_mount_name(self):
        return "data{0:04}".format(self.id)

    def __str__(self):
        default = "" 
        if self.is_default:
            default = " (default)"

        return "data{0:04}{1} on {2}".format(self.id, default, self.petal)

    def get_absolute_url(self):
        return reverse('partition-update', args=[self.id])

class Member(MemberResource):
    """
    The base record - every member in the organization has an entry in the
    Member table and every resource has a foreign key relationship to this
    record.
    """
    BASIC = 1
    STANDARD = 2

    CATEGORY_CHOICES = (
        (BASIC, _('basic')),
        (STANDARD, _('standard')),
    )

    name = models.CharField(unique=True, help_text=_("Friendly name, can be changed later."), max_length=64)
    category = models.SmallIntegerField(choices=CATEGORY_CHOICES, default=STANDARD, help_text=_("Basic memberships gets one login and one email address, standard gets full array of resources."))
    disk_quota = models.PositiveIntegerField(default=50000, help_text=_("Total disk space for the entire membership, in MB."))
    is_active = models.BooleanField(default=True, help_text=_("Whether or not the membership is active, when disabled, all related resources are disabled."))

    def get_disk_quota_display(self):
        return str('{:01.2f}'.format(self.disk_quota / 1000)) + ' GB'

    def get_related_resource_querysets(self, enable_order=True):
        # Create a list containing every possible resource that is tied to this
        # membership. The list is useful when disabling or deleting memberships
        # to ensure we have disabled or deleted all the children.
        querysets = []
        for resource in get_all_member_resource_classes(enable_order=enable_order):
            querysets.append(resource.objects.filter(member=self.id))
        return querysets

    def delete(self, *args, **kwargs):
        # If we simply delete the member, our cascading deletes will properly
        # remove all the related items from the database, but they will remain
        # on the servers. So, before deleting we have to individually delete
        # every related resource so it triggers a submit to the appropriate
        # server so they can all be cleanly removed.
        for queryset in self.get_related_resource_querysets(enable_order=False):
            for resource in queryset:
                resource.delete()

        super().delete(*args, **kwargs) 

    def save(self, *args, **kwargs):

        """ 
        Whenever we save, we have to check the is_active field. If it's
        disabled, ensure all resources are also disabled. When enabled, ensure
        all resources that are auto disabled are enabled.  
        """

        if self.id:
            if self.is_active == True:
                enable_order = True
                resource_is_active = MemberResource.IS_ACTIVE
            else:
                enable_order = False 
                resource_is_active = MemberResource.IS_DISABLED_AUTO

            for queryset in self.get_related_resource_querysets(enable_order=enable_order):
                for resource in queryset:
                    # If we are set to something else (and we are not manually disabled) then we update.
                    if resource.is_active != resource_is_active and resource.is_active != MemberResource.IS_DISABLED_BY_USER:
                        resource.is_active = resource_is_active
                        resource.save()

        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('member-update', self.id)

    def __str__(self):
        return self.name

class DomainZone(MemberResource):
    """
    The DomainZone is the bare/root domain name, e.g. workingdirectory.net.
    You must first specify a DomainZone, then you can create DomainNames 
    (e.g. A workingdirectory.net 1.2.3.4) that have a foreign key relationship
    back to the DomainZone.
    """
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    petals = models.ManyToManyField(Petal, through='DomainZoneRunner')
    domain = models.CharField(max_length=254, unique=True, validators=[valid_domain_zone])

    def clean(self):
        super().clean()

        psl = PublicSuffixList()
        private_suffix = psl.privatesuffix(self.domain)
        if "www." + private_suffix == self.domain: 
            raise ValidationError(
                _("Please leave out the www part of the domain name and just enter the root part (%(value)s)"), params={'value': private_suffix},
            )

        # By default, we verify domain suffixes to ensure they are private
        # suffixes (based on the public suffix list). This prevents users from
        # creating a domain zone like com.uk which would prevent any other
        # member from register a perfectly acceptable domain like myorg.com.uk.
        # However, we make an exception for the host organization, which may
        # control some of it's own public suffixes.
        verify_private_suffix = True 
        if self.member_id == settings.FLOWER_HOST_MEMBER_ID:
            if self.domain in settings.FLOWER_HOST_ALLOWED_PUBLIC_SUFFIXES:
                verify_private_suffix = False 

        if private_suffix == None and verify_private_suffix:
            raise ValidationError(
                _("The domain zone you entered (%(value)s) does not contain a non-public part. Did you forget to add .org or .net?"),
                params={'value': value},
            )
        # Ensure shortest private suffix is not being used by another member. 
        result = DomainZone.objects.filter(domain__endswith=private_suffix).exclude(member_id=self.member_id)
        if result:
            raise ValidationError( _("That domain is in use by another member.") )

        
    def save(self, *args, **kwargs):
        """
        Whenever a new domain zone is created, we help out the user by trying to create:
            1. MX records pointing to our MX hosts. 
            2. Two A records, one for the bare domain, one for www.
        """
        if self.id:
            # If this is not a new record, simply save and return.
            super().save(*args, **kwargs)
            return

        super().save(*args, **kwargs)
        member_id = self.member_id
        domain_zone_id = self.id

        # Set MX records to our default MX domains. 
        dn_params = {
            'category': DomainName.MX,
            'member_id': member_id,
            'domain_zone_id': domain_zone_id,
            'distance': 10,
        }
        distance = 0
        for mx_domain in settings.FLOWER_MX_DOMAINS:
            dn_params['server_name'] = mx_domain
            dn_params['distance'] = distance + 10
            DomainName.objects.create(**dn_params)

        # Create a website cache record for the bare domain.
        dn_params = {
            'category': DomainName.WEBPROXY,
            'member_id': member_id,
            'domain_zone_id': domain_zone_id,
            'server_name': settings.FLOWER_WEBPROXY_DEFAULT_SERVER_NAME
        }
        DomainName.objects.create(**dn_params)
        # And again with the label www.
        dn_params['label'] = 'www';
        DomainName.objects.create(**dn_params)

    def delete(self, *args, **kwargs):
        # Deleting a zone will result in all related domain names being
        # deleted, which is good and automatic. But... we have to be sure that
        # any MX domain names have been properly removed from our LDAP server
        # as well.
        for dn in DomainName.objects.filter(domain_zone=self.id, category=DomainName.MX, is_active=True):
            if dn.label:
                domain_name = dn.label + "." + dn.domain_zone.domain
            else:
                domain_name = dn.domain_zone.domain
            self.setup_mx_domain_runner(desired_status=RunnerCommon.STATUS_DELETED, domain_name=domain_name)

        super().delete(*args, **kwargs) 

    def get_runner_object(self=None):
        return DomainZoneRunner

    """
    MX Domain runners are a special case for a domain zone. Each time we create
    an MX record, we have to update our LDAP server to indicate that we should
    accept email for the domain name (and when it is deleted, that record should
    be removed from LDAP).
    """
    def setup_mx_domain_runner(self, desired_status=None, domain_name=None):
        data = {
            "domain_name": domain_name
        }
        if desired_status == RunnerCommon.STATUS_ACTIVE:
            petal_action = RunnerCommon.PETAL_ACTION_ADD_EMAIL_DOMAIN
        else:
            petal_action = RunnerCommon.PETAL_ACTION_REMOVE_EMAIL_DOMAIN

        petals = self.get_petals_for_category(Petal.LDAP)
        self.retire_active_runners(Petal.LDAP)
        self.create_runners(petals=petals, desired_status=desired_status, petal_action=petal_action, data=data)

    def setup_runners(self, desired_status=None):
        # Gather the data.
        data = { 
            'primary_name_server': None,
            'zone': self.domain,
            'records': []
        }

        if desired_status == RunnerCommon.STATUS_DELETED:
            # We don't need any more data to delete - just the zone.
            petal_action = RunnerCommon.PETAL_ACTION_DELETE_DOMAIN_ZONE

        else:
            # For create and disable we do the same thing. I know, it's not really
            # disabling but... if we delete the records, then members have no idea
            # why their web site is not working. If we keep the DNS records, then
            # they get redirected to a web page that says their web site is disabled.
            petal_action = RunnerCommon.PETAL_ACTION_CREATE_DOMAIN_ZONE

            # Initialize with NS records.
            for name_server in settings.FLOWER_NAME_SERVERS:
                # We consider the first name server listed to be the primary.
                if data['primary_name_server'] == None:
                    data['primary_name_server'] = name_server

                record = {
                    'category': DomainName.ZONE_FILE_CATEGORY_MAP[DomainName.NS],
                    'ttl': 86400,
                    'server_name': name_server
                }
                data['records'].append(record)

            for domain_name in DomainName.objects.filter(domain_zone=self, is_active=True):
                category = domain_name.category
                if category == DomainName.WEBPROXY:
                    # Convert record to A record.
                    # FIXME should be lookup, not sure how to bootstrap.
                    ip_address = "172.18.0.15"
                    category = DomainName.A
                else:
                    ip_address = domain_name.ip_address

                record = {
                    'category': domain_name.ZONE_FILE_CATEGORY_MAP[category],
                    'label': domain_name.label,
                    'ip_address': ip_address,
                    'ttl': domain_name.ttl,
                    'server_name': domain_name.server_name,
                    'distance': domain_name.distance,
                    'port': domain_name.port,
                    'weight': domain_name.weight,
                    'sshfp_type': domain_name.sshfp_type,
                    'sshfp_algorithm': domain_name.sshfp_algorithm,
                    'sshfp_data': domain_name.sshfp_data
                }
                data['records'].append(record)

        petals = self.get_petals_for_category(Petal.NSAUTH)
        self.retire_active_runners(Petal.NSAUTH)
        self.create_runners(petals=petals, desired_status=desired_status, petal_action=petal_action, data=data)
        
        # For each MX domain name that points to us, we have to ensure postfix is configured
        # to accept email for that domain.

    def __str__(self):
        return self.domain

    def get_absolute_url(self):
        return reverse('domain-name-list', args=[self.member.id, self.id])

class DomainZoneRunner(RunnerCommon):
    resource = models.ForeignKey(DomainZone, on_delete=models.SET_NULL, blank=True, null=True)
    petal = models.ForeignKey(Petal, on_delete=models.CASCADE)

    def submit(self, force=False):
        # Unlke other resources, we don't submit on every change. A DomainZone gets updated
        # every time a single domain name is updated. Often you might add 10 domain names in
        # a row. Rather than trigger a zone update with each change, we run a cron job every
        # minute to pull them all in together (with force=True). Sadly, we have one ugly
        # hacky exception - when testing or in dev mode, we submit so our tests work.
        if force or self.resource.domain.endswith('.dev') or self.resource.domain.endswith('.test'):
            super().submit()

class DomainName(MemberResource):
    """
    Each DNS entry has a DomainName record.
    """

    MX = 1
    CNAME = 2
    A = 3
    PTR = 4
    TXT = 5
    SRV = 6
    AAAA = 7
    WEBPROXY = 8
    NS = 9
    SSHFP = 10

    # Users are not allowed to make NS records, they are created
    # automatically.
    CATEGORY_CHOICES = (
        (A, 'a'),
        (MX, 'mx'),
        (CNAME, 'cname'),
        (PTR, 'ptr'),
        (TXT, 'txt'),
        (SRV, 'srv'),
        (AAAA, 'aaaa'),
        (SSHFP, 'sshfp'),
        (WEBPROXY, 'webproxy'),
    )

    # Map each category to the value that should be
    # inserted into a zone file. This map is different
    # from the map used to translate categories to human
    # readable names (see CATEGORY_CHOICES above).
    ZONE_FILE_CATEGORY_MAP = {
        MX: "MX",
        CNAME: "CNAME",
        A: "A",
        PTR: "PTR",
        TXT: "TXT",
        SRV: "SRV",
        AAAA: "AAAA",
        NS: "NS",
        SSHFP: "SSHFP"
    }

    SSHFP_ALGORITHM_RSA = 1
    SSHFP_ALGORITHM_DSA = 2
    SSHFP_ALGORITHM_ECDSA = 3
    SSHFP_ALGORITHM_ED25519 = 4

    SSHFP_ALGORITHM_CHOICES = (
        (0, ''),
        (SSHFP_ALGORITHM_RSA, 'rsa'),
        (SSHFP_ALGORITHM_DSA, 'dsa'),
        (SSHFP_ALGORITHM_ECDSA, 'ecdsa'),
        (SSHFP_ALGORITHM_ED25519, 'ed25519'),
    )

    SSHFP_TYPE_SHA1 = 1
    SSHFP_TYPE_SHA256 = 2

    SSHFP_TYPE_CHOICES = (
        (0, ''),
        (SSHFP_TYPE_SHA256, 'sha256'),
        (SSHFP_TYPE_SHA1, 'sha1'),
    )

    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    domain_zone = models.ForeignKey(DomainZone, on_delete=models.CASCADE, help_text=_("The base domain, e.g. mayfirst.coop or fightback.org."))
    category = models.SmallIntegerField(choices=CATEGORY_CHOICES, blank=False, help_text=_("What category of domain name record do you want? An A record is commonly used for setting up web sites. An MX record is used to configure where your email should go. A TXT record can be used for SPF or DKIM. A WEBPROXY is a custom type used only for web sites hosted with us."))
    label = models.CharField(max_length=254, blank=True, validators=[valid_domain_label], help_text=_("The sub domain, the part before the first dot. Often left blank for MX records and for A records designating the web site."))
    ip_address = models.GenericIPAddressField(blank=True, null=True, verbose_name=_("IP Address"))
    ttl = models.PositiveIntegerField(default=3600, help_text=_("The time to live indicates how many seconds it will take before changes to the record are fully propagated. The number is in seconds - 3600 seconds, or one hour, is recommended. Must be 300 or greater"), validators=[valid_domain_ttl], verbose_name=_("Time to live"))
    server_name = models.CharField(max_length=254, blank=True, validators=[valid_domain_server_name], help_text=_("Server name is required for MX and SRV records."))
    txt = models.CharField(verbose_name=_("Text"), max_length=254, blank=True, help_text=_("The text to associate with this domain. Required for TXT records."))
    distance = models.PositiveIntegerField(verbose_name=_("Distance"), default=0, help_text=_("If you have more than one MX record specified, the one with the smallest distance is chosen first."))
    port = models.PositiveIntegerField(default=0)
    weight = models.PositiveIntegerField(default=0)
    sshfp_algorithm = models.PositiveIntegerField(verbose_name=_("SSH Fingerprint Algorithm"), choices=SSHFP_ALGORITHM_CHOICES, default=0, validators=[valid_sshfp_algorithm], help_text=_("The algorithm to use, note: dsa is considered insecure and should not be used."))
    sshfp_type = models.PositiveIntegerField(verbose_name=_("SSH Fingerprint Type"), choices=SSHFP_TYPE_CHOICES, default=0, validators=[valid_sshfp_type], help_text=_("The encryption type, note: sha1 is considered insecure and should not be used."))
    sshfp_data = models.CharField(verbose_name=_("SSH Fingerprint data"), max_length=254, blank=True, help_text=_("The SSH Fingerprint associated with this domain (typically generated via 'ssh_keygen -r')."))

    def build_category_map():
        """
        Helper function to build a dictionary style map of database values
        to human readable names. These are written out in the domain name
        form template to be used by our javascript code.
        """
        ret = [] 
        for key, label in DomainName.CATEGORY_CHOICES:
            ret.append({'key': key, 'label': label})

        return ret

    def is_mailing_list_domain(self):
        if MailingListDomainMap.objects.filter(domain_name__label=self.label, domain_name_id=self.id).exists():
            return True
        return False

    """
    You can have multiple MX records. This function checks to see if this record
    is the last MX record with the same label.
    """
    def is_last_mx_record(self):
        # We only care about MX records here.
        if self.category != self.MX:
            return False

        # Count how many records exists with this label and domain zone.
        dns = DomainName.objects.filter(category=self.category, label=self.label, domain_zone=self.domain_zone).count()
        if dns > 1:
            # We are not the last, let the user do what they want.
            return False
        # We are the last.
        return True

    def is_last_mx_record_used_by_email_address(self):
        if not self.is_last_mx_record():
            # We are not the last MX record, so it doesn't matter if we are in use by
            # an email address.
            return False

        # There is only one matching record left! See if it is in use.
        domain_name = ""
        if self.label:
            domain_name += self.label + "."
        domain_name += self.domain_zone.domain

        if EmailAlias.objects.filter(domain_name=domain_name).exists():
            return True
        if EmailMailbox.objects.filter(domain_name=domain_name).exists():
            return True

        return False

    def get_edit_disabled_attribute(self):
        if self.is_mailing_list_domain():
            return 'disabled'
        return ""

    def get_delete_disabled_attribute(self):
        if self.is_mailing_list_domain():
            return 'disabled'
        if self.is_last_mx_record_used_by_email_address():
            return 'disabled'
        return ""

    def get_edit_title(self):
        if self.is_mailing_list_domain():
            return _("This record cannot be edited since it is designated as a Mailing List domain.")
        return ""

    def get_delete_title(self):
        if self.is_mailing_list_domain():
            return _("This record cannot be deleted since it is designated as a Mailing List domain.")
        if self.is_last_mx_record_used_by_email_address():
            return _("This record cannot be deleted since it is used by one or more email addresses.")
        return ""

    def valid_ip_and_domain(self, addr):
        """
        Except in special circumstances, the ip address cannot be in a private range.
        """

        domain = self.domain_zone.domain
        if addr.is_private:
            # We allow private IPs for testing and dev purposes, but not in production.
            if not settings.FLOWER_ALLOW_PRIVATE_IPS:
                if domain not in settings.FLOWER_PRIVATE_DOMAINS:
                    raise ValidationError( _("This IP address is in the private range. Please ony set IP addresses to publicly available IP addresses") )

        # Ensure any specially defined private domain names are using the right IP networks.
        if domain in settings.FLOWER_PRIVATE_DOMAINS:
            acceptable_private_ip = False
            for private_network in settings.FLOWER_PRIVATE_NETWORKS:
                net = ipaddress.IPv4Network(private_network)
                if addr in net.hosts():
                    acceptable_private_ip = True

            if acceptable_private_ip == False:
                raise ValidationError( _("That domain is restricted to pre-defined set of private IP addresses.") )

    def valid_ttl(self):
        mismatch = DomainName.objects.filter(domain_zone_id=self.domain_zone.id, label=self.label, category=self.category).exclude(ttl=self.ttl)
        if mismatch:
            raise ValidationError( 
                _("An entry already exists of the same DNS type and domain but with a time to live of %(ttl)s. All matching records must have the same time to live. Please change your time to live to %(ttl)s."), params={ 'ttl': mismatch[0].ttl }
            )

    def clean(self):
        super().clean()

        if self.is_mailing_list_domain():
            raise ValidationError( _("This domain name is being used as a Mailing List Domain so it cannot be added or edited.") )

        addr = None
        if self.ip_address:
            # Since we are using Django's IPAddress field, we should have already
            # caught any invalid IP addresses so this should not fail.
            addr = ipaddress.ip_address(self.ip_address)

        # Knot will complain if two records with the same fqdn and type have different ttls.
        self.valid_ttl()

        # Don't let someone register a domain name using another persons domain zone.
        if self.domain_zone.member_id != self.member_id:
            raise ValidationError( _("The domain zone you chose seems to belong to another member.") )

        if self.ip_address and self.server_name:
            raise ValidationError( _("You cannot set both a server name and IP Address. If you entering an MX record just put the server name.") )

        if self.category == self.A:
            if not self.ip_address:
                raise ValidationError( _("An IP Address is required with A records.") )

            if addr.__class__.__name__ != 'IPv4Address':
                raise ValidationError( _("This IP address does not appear to be an IPV4 address. Did you mean to create an IPV6 address using an AAAA DNS record?") )

            # Check for private IP range. 
            self.valid_ip_and_domain(addr)

        if self.category == self.AAAA:
            if not self.ip_address:
                raise ValidationError( _("An IP Address is required with AAAA records.") )

            if addr.__class__.__name__ != 'IPv6Address':
                raise ValidationError( _("This IP address does not appear to be an IPv6 address. Did you mean to create an IPv4 address using an A DNS record?") )

            if addr.is_private:
                raise ValidationError( _("This IP address is in the private range. Please ony set IP addresses to publicly available IP addresses") )

        if self.category == self.MX:
            if not self.server_name:
                raise ValidationError( _("An MX record must have a server name defined.") )
            if self.distance < 1:
                raise ValidationError( _("An MX record must distance defined and be greater or equal to 1.") )

        if self.category == self.CNAME:
            if not self.server_name:
                raise ValidationError( _("A CNAME record must have a server name defined.") )

            # A CNAME cannot be a bare domain.
            if not self.label:
                raise ValidationError( _("A CNAME must have a label. We don't support CNAME flattening or domain aliases. Please support the ANAME standard!") )
                
            # A CNAME cannot have any other matching fqdn's.
            if DomainName.objects.filter(label=self.label).filter(domain_zone_id=self.domain_zone.id).exclude(id=self.id).exists():
                raise ValidationError( _("A CNAME record cannot co-exist with another domain name with the same label.") )

        if self.category == self.WEBPROXY:
            if not self.server_name:
                raise ValidationError( _("A CNAME record must have a server name defined.") )

            # A WEBPROXY cannot have any other matching fqdn's that are A records.
            if DomainName.objects.filter(label=self.label).filter(domain_zone_id=self.domain_zone.id).filter(category=self.A).exists():
                raise ValidationError( _("A WEBPROXY record cannot co-exist with another domain name with the same label that is an A record.") )
              
            # One WEBPROXY record per domain is enough.
            if DomainName.objects.filter(label=self.label).filter(domain_zone_id=self.domain_zone.id).filter(category=self.WEBPROXY).exclude(id=self.id).exists():
                raise ValidationError( _("A website proxy record for this domain name already exists. One is enough.") )

            # Webproxy records are resolved and converted into A records prior
            # to insertion into the DNS server. They are not designed to be
            # aliasess or flattened CNAMES (i.e. resolved by the authoritative
            # server when requested). Therefore, the serve name should be
            # restricted to domain names controlled by the host.
            if not re.match(settings.FLOWER_WEBPROXY_SERVER_NAME_REGEXP, self.server_name):
                raise ValidationError( _("A webproxy record can only be used for web sites hosted with us and can only be set to our website proxy domains. Try using {0} instead.".format(settings.FLOWER_WEBPROXY_DEFAULT_SERVER_NAME) ) )

 
        if self.category == self.SRV:
            if not self.server_name:
                raise ValidationError( _("A SRV record must have a server name defined.") )
            if not self.port:
                raise ValidationError( _("A SRV record must have a port defined.") )

            # Should be in format: _http._tcp.domain.name.org
            srv_parts = self.server_name.split('.')
            if not srv_parts.count() > 2:
                raise ValidationError( _("A SRV record's server name must have at least three dot separated parts.") )

            # Both first parts must start with an underscore.
            if not srv_parts[0].startswith('_') or not srv_parts[1].startswith('_'):
                raise ValidationError( _("A SRV record's server name must be in the format _service._protocol.domain.org - the first two parts must start with an underscore.") )
            # Only supported protocols are tcp, udp, and tls.
            allowed_protocols = [ "tcp", "udp", "tls" ]
            if not srv_parts[1] in allowed_protocols:
                raise ValidationError( _("The protocol (second part after the service) must be either _tcp or _udp or _tls.") )

        if self.category == self.TXT:
            if not self.txt:
                raise ValidationError( _("A TXT record must have a txt value defined.") )

            if '"' in self.txt:
                raise ValidationError( _("A TXT record cannot contain a double quote.") )

        if self.category != self.SSHFP:
            if self.sshfp_algorithm != 0:
                raise ValidationError( _("Only SSHFP records should have an sshfp algorithm greater then 0.") )
            if self.sshfp_type != 0:
                raise ValidationError( _("Only SSHFP records should have an sshfp type greater then 0.") )
            if self.sshfp_data:
                raise ValidationError( _("Only SSHFP records should have the data field filled in.") )

        if self.category == self.SSHFP:
            if self.sshfp_algorithm == 0:
                raise ValidationError( _("Please set a valid sshfp algorithm.") )
            if self.sshfp_type == 0:
                raise ValidationError( _("Please set a valid sshfp type.") )
            if not self.sshfp_data:
                raise ValidationError( _("Please enter an sshfp fingerprint in the SSHFP data field.") )
            if self.sshfp_type == self.SSHFP_TYPE_SHA1 and not re.match('^[A-Fa-f0-9]{40}$', self.sshfp_data):
                raise ValidationError( _("A sha1 hash should be only hexadecimal and exactly 40 characters.") )
            if self.sshfp_type == self.SSHFP_TYPE_SHA256 and not re.match('^[A-Fa-f0-9]{64}$', self.sshfp_data):
                raise ValidationError( _("A sha256 hash should be only hexadecimal and exactly 64 characters.") )

        if self.category == self.PTR:
            if not self.ip_address:
                raise ValidationError( _("A PTR record must have an IP address value defined.") )

            # Check for private IP range. 
            self.valid_ip_and_domain(addr)

            # Ensure it's in the range we own.
            acceptable_ip = False
            if type(addr) == 'IPv6Address':
                allowed_networks = settings.FLOWER_IPV6_PTR_NETWORKS
            else:
                allowed_networks = settings.FLOWER_IPV4_PTR_NETWORKS

            for ipv6_network in settings.FLOWER_IPV6_NETWORKS:
                net = ipaddress.IPv6Network(ipv6_network)
                if addr in net.hosts():
                    acceptable_ip = True

            if acceptable_ip == False:
                raise ValidationError( _("We can only set pointer records for ip addresses in our network.") )

    def setup_runners(self, desired_status=None):
        # We don't create our own runners. Instead, when a single domain name
        # is modified, we create a DomainZone runner so the entire zone file
        # can be updated.
        domain_zone = DomainZone.objects.get(pk=self.domain_zone_id)

        # Ensure we preserve the current status of the domain zone (we don't want to
        # re-enable a disabled domain zone by adding a new record).
        if domain_zone.is_active == domain_zone.IS_ACTIVE:
            desired_zone_status = RunnerCommon.STATUS_ACTIVE
        else:
            desired_zone_status = RunnerCommon.STATUS_DISABLED

        domain_zone.setup_runners(desired_status=desired_zone_status)

        # Now for a special case. If this is an MX record we need to either
        # create a domain name email record in LDAP (or delete it).
        if self.category == self.MX:
            # When deleting or disabling we only want to remove the domain name
            # email record if we are the last MX domain.
            if desired_status == RunnerCommon.STATUS_DISABLED or desired_status == RunnerCommon.STATUS_DELETED:
                if not self.is_last_mx_record():
                    # Never mind, we are deleting or disabling an MX record and there are others still around.
                    return

            if self.label:
                domain_name = self.label + "." + self.domain_zone.domain
            else:
                domain_name = self.domain_zone.domain

            domain_zone.setup_mx_domain_runner(desired_status=desired_status, domain_name=domain_name)

    def get_absolute_url(self):
        return reverse('domain-name-update', args=[self.member.id, self.id ])

    def __str__(self):
        if self.label:
            return self.label + '.' + str(self.domain_zone)
        else:
            return str(self.domain_zone)

class MailingListDomain(MemberResource):
    """
    The MailingListDomain allows us to designate a label + DomainZone that
    should be reserved for MailingLists (e.g. lists.workingdirectory.net). The
    main purpose is to know how to populate the list of available DomainNames
    when presenting the user with a form to either create a new EmailAlias,
    EmailMailbox or create a new MailingList. When generating a list of
    Domains for an EmailAlias or EmailMailbox, we show all MX DomainNames
    but exclude any listed as MailingListDOmains. Alternately, when presenting
    a list of MailingListDomains, we can pull directly from this table.

    When you create a MailingListDomain, we automatically create two DomainName
    records: one is an MX record and the other is an A record. We keep track of
    which DomainName records belong to this MailingListDomain entry by populating
    the MailingListDomainMap table (see below).
    """
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    domain_zone = models.ForeignKey(DomainZone, on_delete=models.CASCADE, help_text=_("Choose the domain zone you want to use for your mailing list."))
    label = models.CharField(max_length=254, blank=True, help_text=_("Typically, mailing list domain names use 'lists' as the label, so the complete domain name is something like lists.group.org."))

    def clean(self):
        super().clean()
        if not self.id:
            # Make sure the fqdn does not already exist if this is a new record.
            domains = DomainName.objects.filter(label=self.label, domain_zone=self.domain_zone.id)
            if domains:
              raise ValidationError( _("A domain name already exists with that label. Please choose a different label or delete that domain name from your domain name records and try again.") )
        # Don't let someone register with another persons domain zone.
        if self.member_id != self.domain_zone.member_id:
            raise ValidationError( _("The domain zone you chose seems to belong to another member.") )

    def save(self, *args, **kwargs):
        """
        Whenever a new mailing list domain is created, we help out the user by trying to create:
            1. MX records pointing to our MX hosts. 
            2. An A record pointing to our website cache.
            3. Corresponding entries in the MailingListDomainMap table so
               we can properly track and clean up these records.
        """
        if self.id:
            # If this is not a new record, simply save and return.
            super().save(*args, **kwargs)
            return

        super().save(*args, **kwargs)
        member_id = self.member_id
        domain_zone_id = self.domain_zone_id
        label = self.label

        # Track these two variables so we can make MailingListDomainMap
        # entries when we are done.
        mailing_list_domain_id = self.id
        domain_name_ids = []

        # Set MX records to our default MX domains. 
        dn_params = {
            'category': DomainName.MX,
            'member_id': member_id,
            'domain_zone_id': domain_zone_id,
            'label': label,
            'server_name': 'a.mx.mayfirst.org', 
            'distance': 10,
        }
        dn = DomainName.objects.create(**dn_params)
        domain_name_ids.append(dn.id)

        # Now with a secondary MX server.
        dn_params['server_name'] = 'b.mx.mayfirst.org';
        dn_params['distance'] = 20;
        dn = DomainName.objects.create(**dn_params)
        domain_name_ids.append(dn.id)

        # Create a website cache record.
        dn_params = {
            'category': DomainName.WEBPROXY,
            'member_id': member_id,
            'label': label,
            'domain_zone_id': domain_zone_id,
            'server_name': settings.FLOWER_WEBPROXY_DEFAULT_SERVER_NAME
        }
        DomainName.objects.create(**dn_params)
            
        # Create a MailingListDomainMap record for each DomainName record
        # that we created so we can clean them up when we're done and also
        # to ensure users don't modify or delete them.
        for domain_name_id in domain_name_ids:
            map_params = {
                'domain_name_id': domain_name_id,
                'mailing_list_domain_id': mailing_list_domain_id
            }
            MailingListDomainMap.objects.create(**map_params)

    def delete(self, *args, **kwargs):
        """
        Delete related DomainNames first.
        """
        domain_maps = MailingListDomainMap.objects.filter(mailing_list_domain=self.id)
        # First delete the map record, otherwise we won't be able to
        # delete the Domain Name record (it will fail validation).
        for domain_map in domain_maps:
            instance = MailingListDomainMap.objects.get(domain_name_id=domain_map.domain_name_id)
            instance.delete()
            
        # Now we can delete the DomainName record.
        for domain_map in domain_maps:
            instance = DomainName.objects.get(id=domain_map.domain_name.id)
            instance.delete()

        super().delete(*args, **kwargs) 

    def __str__(self):
        if self.label:
            return self.label + "." + str(self.domain_zone)
        else:
            return str(self.domain_zone)

    def get_absolute_url(self):
        return reverse('mailing-list-domain-update', args=[self.member.id, self.id])

class MailingListDomainMap(models.Model):
    """
    Simple mapping table to track which DomainName entries are controlled
    by the MailingListDomain record. By mapping them explicitly, we can
    more effectively ensure that users don't edit or delete any DomainName
    entries that are controlled by the MailingListDomain and also ensure that
    if the MailingListDomain entry is deleted, we delete the associated DomainName entries as well.  
    """ 

    domain_name = models.OneToOneField(DomainName, on_delete=models.CASCADE, primary_key=True)
    mailing_list_domain = models.ForeignKey(MailingListDomain, on_delete=models.CASCADE)

class Contact(MemberResource):
    """
    Contact records allow members to control who should have full access to their membership.
    The Contact records are sent all password reset emails and are synchornized with our
    membership database.
    """
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    email = models.EmailField(max_length=254)
    is_billing = models.BooleanField(default=True)

    def get_absolute_url(self):
        return reverse('contact-update', args=[self.member.id, self.id])

    def __str__(self):
        return self.first_name + " " + self.last_name

class MailingList(MemberResource):
    """
    A mailing list create a Mailman list.
    """
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    petals = models.ManyToManyField(Petal, through='MailingListRunner')
    listname = models.CharField(validators=[valid_user_portion], max_length=64, unique=True, help_text=_("The listname is the portion of the email list that comes before the @ sign. If your list address is support@lists.mayfirst.org, the list name would be: support."))
    mailing_list_domain = models.ForeignKey(MailingListDomain, on_delete=models.CASCADE, help_text=_("Select from the list of domains that are designated to be used for email lists."))

    def clean(self):
        super().clean()
        # Don't let someone register a mailing list using another persons mailing list domain.
        if self.member_id != self.mailing_list_domain.member_id:
            raise ValidationError( _("The mailing list domain you chose seems to belong to another member.") )

    def get_absolute_url(self):
        return reverse('mailing-list-update', args=[self.member.id, self.id])

    def __str__(self):
        return self.listname + '@' + str(self.mailing_list_domain)

class MailingListRunner(RunnerCommon):
    resource = models.ForeignKey(MailingList, on_delete=models.SET_NULL, blank=True, null=True)
    petal = models.ForeignKey(Petal, on_delete=models.CASCADE)


class Login(MemberResource):
    """
    Login is the single place for all username and password combinations. 
    Each login can optionally be configured to allow various access to other
    resources, such as SSH, Nextcloud, XMPP, email etc.
    """
    AUTO_RESPONSE_OFF = 1
    AUTO_RESPONSE_REPLY_AND_DELIVER = 2
    AUTO_RESPONSE_REPLY_AND_DISCARD = 3

    AUTO_RESPONSE_CHOICES = (
        (AUTO_RESPONSE_OFF, _('Do not send auto response')),
        (AUTO_RESPONSE_REPLY_AND_DELIVER, _('Send auto response and accept incoming email')),
        (AUTO_RESPONSE_REPLY_AND_DISCARD, _('Send auto response and discard incoming email')),
    )

    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    petals = models.ManyToManyField(Petal, through='LoginRunner')
    username = models.CharField(max_length=254, unique=True, validators=[valid_username], help_text=_("Please use only lower case letters, numbers, periods, and dashes."))
    password = models.CharField(max_length=254, blank=True, validators=[valid_password])
    uid = models.PositiveIntegerField(unique=True, blank=True)
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64, blank=True, verbose_name=_("Last Name (optional)"))
    shell_access = models.BooleanField(default=False, help_text=_("Shell Access means this user will be able to login to one of our servers to copy files or run commands. Shell access is typically only required for users who maintain web sites."))
    ssh_key = models.TextField(blank=True, validators=[valid_ssh_public_key_file], help_text=_("An SSH public key allows users to login to our servers without exchanging a password. Please upload the public key, not the private one."), verbose_name = _('SSH Public Key'))
    nextcloud_access = models.BooleanField(default=False, help_text=_('Nextcloud is our document sharing, calendar and contacts application that is available via <a href="https://share.mayfirst.org/">share.mayfirst.org</a>.'))
    nextcloud_quota = models.PositiveIntegerField(default=5000, help_text=_("Enter the Nextcloud disk quota for this user in megabytes."))
    discourse_access = models.BooleanField(default=False, help_text=_('Discourse is our online community forum site available via <a href="https://comment.mayfirst.org/">comment.mayfirst.org</a>'))
    xmpp_access = models.BooleanField(default=False, verbose_name=_("XMPP Access"), help_text=_('XMPP is our instant messanger program - for instructions, please see our <a href="https://support.mayfirst.org/wiki/how-tos/jabber">online help page</a>.'))
    livestreaming_access = models.BooleanField(default=False, help_text=_('All members can live video stream directly from their cell phones via <a href="https://live.mayfirst.org">live.mayfirst.org</a>.'))
    auto_response_action = models.SmallIntegerField(choices=AUTO_RESPONSE_CHOICES, default=AUTO_RESPONSE_OFF)
    auto_response_reply_from = models.EmailField(max_length=254, blank=True, help_text=_('Enter the email address the auto response should come from.'))
    auto_response = models.TextField(blank=True)
    recovery_email = models.EmailField(max_length=254, blank=True, help_text=_('Enter an email address that will be sent a reset link if you ever lose your password.'))
    mailbox_quota = models.PositiveIntegerField(default=1000, help_text=_("Enter the size limit of the mailbox in megabytes."))
    mail_store = models.ForeignKey(Petal, related_name="logins", on_delete=models.SET_NULL, blank=True, null=True, limit_choices_to={'category': Petal.MAILSTORE}, help_text=_("The canonical mail host holding this mailbox. Set automatically to default if left blank, only change-able by admins."))
    partition = models.ForeignKey(Partition, related_name="logins", on_delete=models.SET_NULL, blank=True, null=True, limit_choices_to=models.Q(petal__category=Petal.MAILSTORE), help_text=_("The partition holding this mailbox's data. Set automatically to default if left blank, only change-able by admins."))

    def clean(self):
        super().clean()

        # If we have an id (which means we are updating not saving) ensure that
        # the username has not changed. Changing the username can cause all
        # categorys of grief in apps (like Nextcloud) that depend on it staying
        # the same.
        if self.id:
            existing = Login.objects.get(id=self.id)
            if existing.username != self.username:
                raise ValidationError( _("You may not change the username of an existing login. Please create a new one instead.") )

        if not member_under_quota(self.member_id, self.mailbox_quota + self.nextcloud_quota, self.id, 'Login'):
            raise ValidationError( _("You have exceeded your total membership quota. Please adjust your nextcloud quota or mailbox quota. Or, submit a request to increase your membership quota.") )

        if self.mail_store:
            if not Petal.objects.filter(pk=self.mail_store.id).exists():
                raise ValidationError( _("The mail store server does not exist in the petals table.") )


    def get_absolute_url(self):
        return reverse('login-update', args=[self.member.id, self.id])

    def delete(self, using=None, keep_parents=False):
        # Delete the user from the django user system.
        User.objects.filter(username=self.username).delete()

        # Now, delete from the database. Adios.
        super().delete(using, keep_parents)

    def save(self, *args, **kwargs):
        if not self.mail_store:
            # Use default MAILSTORE.
            self.mail_store = Petal.objects.get(is_default=True, category=Petal.MAILSTORE)

        if not self.partition:
            # Use default partition.
            self.partition = Partition.objects.get(is_default=True, petal=self.mail_store)

        """
        Password!

        We pass the clear text password onto the Django user system where it is
        converted to a django compatible encryption format and saved in the
        user table so the user can login to the control panel. Even if they
        don't have access to resources, they will be able to change their
        password or set vacation messages.

        Then, we use an LDAP compatible encryption algorithm to store the
        encrypted password in the login table itself. This is the password
        that will be passed on to the LDAP server.

        """

        clear_text_password = None
        if not self.id:
            if not self.password:
                # This is a new login and the user did not choose a password.
                # Auto generate a random password.
                clear_text_password = utils.get_random_password()
            else:
                # This is a new user, providing their own password.
                clear_text_password = self.password 

        else:
            # Check for existing password that is not already encrypted.
            if self.password and not re.match('^\$6\$.{103,103}$', self.password):
                # This is an existing record, and the user is submitting a new
                # password. Store it in the clear_text_password variable so it
                # gets handled properly.
                clear_text_password = self.password

        if clear_text_password:
            # We want to save an LDAP compatible password hash derived from the
            # clear text password on the object, so it goes into the Login table.
            salt = crypt.mksalt(method=crypt.METHOD_SHA512)
            self.password = crypt.crypt(clear_text_password, salt)

        # Check for UID. If it's a new record, we won't have it so we should
        # set it to the next available one. In the process we save this record
        # (with the new LDAP encrypted password).
        if not self.uid:
            with transaction.atomic():
                last_login = Login.objects.select_for_update().order_by('-uid')
                if last_login:
                    last_login = last_login[0]
                    self.uid = last_login.uid + 1 
                    super().save(*args, **kwargs)
                else:
                    self.uid = 1500
                    super().save(*args, **kwargs)
        else:
            # We already have a uid, so perform a normal save.
            super().save(*args, **kwargs)

        # Manage the django account.
        # Every time we save a Login we may need to update the django
        # user account (because we may be enabling a disabled account).
        # Try to retrieve an existing user.
        try:
            u = User.objects.get(username=self.username)
            if clear_text_password:
                u.set_password(clear_text_password)
            if self.is_active == self.IS_ACTIVE:
                u.is_active = True
            else:
                u.is_active = False

            # Always set the email - the user may be setting or
            # trying to delete their recovery email.
            u.email = self.recovery_email
            u.save()
        except ObjectDoesNotExist:
            # If the user doesn't exist in Django, create it.
            # We always expect to have a clear text password on new accounts and we expect
            # to have is_active=True since you can't create a disabled new account.
            User.objects.create_user(self.username, self.recovery_email, clear_text_password)

        
    def get_runner_object(self=None):
        return LoginRunner

    def setup_runners(self, desired_status=None):
        # Create LDAP runner.
        petal_action = None
        if desired_status == RunnerCommon.STATUS_ACTIVE:
            petal_action = RunnerCommon.PETAL_ACTION_CREATE_LDAP_LOGIN
            data = {
                'username': self.username,
                'first_name': self.first_name,
                'last_name': self.last_name,
                'password': self.password,
                'uid': self.uid
            }
        else:
            # We only need username to delete or disable.
            data = { "username": self.username }

            if desired_status == RunnerCommon.STATUS_DELETED:
                petal_action = RunnerCommon.PETAL_ACTION_DELETE_LDAP_LOGIN
            else:
                petal_action = RunnerCommon.PETAL_ACTION_DISABLE_LDAP_LOGIN

        petals = self.get_petals_for_category(Petal.LDAP)
        self.retire_active_runners(Petal.LDAP)
        self.create_runners(petals=petals, petal_action=petal_action, desired_status=desired_status, data=data)

    def __str__(self):
        return self.username

class LoginRunner(RunnerCommon):
    resource = models.ForeignKey(Login, on_delete=models.SET_NULL, blank=True, null=True)
    petal = models.ForeignKey(Petal, on_delete=models.CASCADE)

class PasswordReset(models.Model):
    """
    Keep track of password reset hashes and requests. When a password reset has
    been used, set expiration_date to Null so it can't be used again. Set notify
    to true if you want the recovery email in the related login to get an email
    with the password reset link.
    """
    login = models.ForeignKey(Login, on_delete=models.CASCADE)
    expiration_date = models.DateTimeField(null=True)
    code = models.CharField(max_length=64)
    notify = models.BooleanField(default=False)

    def __str__(self):
        return self.login + " " + _("expiring on") + " " + self.expiration_data

    def send_email(self):
        subject = _("May First password reset")
        message = textwrap.wrap(_("Someobody, hopefully you, requested a password reset for the username %(username)s. Click the link below to reset your password. The link will be valid until %(expiration)s." % { 'username': self.login, 'expiration': self.expiration_date.strftime("%b %d %Y %H:%M:%S") }) + "\n\n" + settings.FLOWER_BASE_URL + self.code, 70)
        print(message)

    def save(self, *args, **kwargs):
        if not self.id:
            # Set defaults on a new record.
            self.expiration_date = timezone.now() + timezone.timedelta(days=settings.FLOWER_PASSWORD_RESET_VALID_DAYS)
            self.code = utils.get_random_url_friendly_code()
            
        super().save(*args, **kwargs)
        if self.notify:
            self.send_email()

    def clean(self):
        super().clean()
        if self.notify and not self.login.recovery_email:
            raise ValidationError( _("I can't notify the user because the login does not have a recovery email set.") )


class EmailMailbox(MemberResource):
    """
    The Email Mailbox table holds email addresses that will route email to the
    mailbox belonging to the related Login account.
    """
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    petals = models.ManyToManyField(Petal, through='EmailMailboxRunner')
    login = models.ForeignKey(Login, on_delete=models.CASCADE, help_text=_("The login to which all all email should be delivered."))
    local = models.CharField(max_length=254, validators=[valid_user_portion], help_text=_("The portion of the email address before the @ sign."))
    domain_name = models.CharField(max_length=254, validators=[valid_domain_name], help_text=_("The portion of the email address after the @ sign."))

    def clean(self):
        super().clean()
        # Don't let someone register an email using another member's login.
        if self.member_id != self.login.member_id:
            raise ValidationError( _("The login you chose seems to belong to another member.") )

        # Don't let someone register an email using another persons domain zone.
        psl = PublicSuffixList()
        private_suffix = psl.privatesuffix(self.domain_name)
        result = DomainZone.objects.filter(domain__endswith=private_suffix).exclude(member_id=self.member_id)
        if result:
            raise ValidationError( _("That domain is in use by another member.") )

        # Don't allow two records with the same email address.
        if EmailMailbox.objects.filter(local=self.local, domain_name=self.domain_name).exclude(id=self.id).exists():
            raise ValidationError( _("An email mailbox with that email address already exists.") )
        if EmailAlias.objects.filter(local=self.local, domain_name=self.domain_name).exists():
            raise ValidationError( _("An email alias with that email address already exists.") )

    def get_type(self):
        return 'mailbox'

    def get_display_type(self):
        return _("Mailbox")

    def get_display_destination(self):
        return self.login

    def get_runner_object(self=None):
        return EmailMailboxRunner 

    def get_default_petals(self, category):
        if category == Petal.MAILSTORE:
            # Don't pull in the default, pull in the petal that the
            # related login is using. This is the server that we should
            # ensure has a Maildir.
            return [ self.login.mail_store ]
        else:
            return super().get_default_petals(category)

    def setup_runners(self, desired_status=None):
        # We need to create resources on MAILSTORE and LDAP petals.

        # Adding/Removing email addresses is complicated because when we add
        # the first email address for a given login, we have to convert the
        # LDAP object by adding the PostfixAccount class and create a Maildir.
        # This part is handled on the petalhub - by always checking if we need
        # to add the PostfixAccount class before trying to add an email
        # address.  In addition, if it's the last email address being removed,
        # we have to remove that class and cleanup that Maildir. That action we
        # call explicitly.
        we_are_last_mailbox = not EmailMailbox.objects.filter(login=self.login.id, is_active=True).exclude(id=self.id).exists()

        # Create MAILSTORE record. We only need to ensure that there
        # is a Maildir created for the given login name and it's owned
        # by the login UID. We also need the partition id.
        petal_action = None
        data = { 
            'username': self.login.username,
            'partition': self.login.partition.get_mount_name(),
        }

        if desired_status == RunnerCommon.STATUS_ACTIVE:
            petal_action = RunnerCommon.PETAL_ACTION_CREATE_MAILDIR

        elif desired_status == RunnerCommon.STATUS_DISABLED:
            # An email is disabled in other ways, we don't want to touch the
            # Maildir so when it is re-enabled it is still functional with all
            # email intact.
            petal_action = RunnerCommon.PETAL_ACTION_NOOP
        elif desired_status == RunnerCommon.STATUS_DELETED:
            if we_are_last_mailbox:
                petal_action = RunnerCommon.PETAL_ACTION_DELETE_MAILDIR
            else:
                petal_action = RunnerCommon.PETAL_ACTION_NOOP

        petals = self.get_petals_for_category(Petal.MAILSTORE)
        self.retire_active_runners(Petal.MAILSTORE)
        self.create_runners(petals=petals, desired_status=desired_status, petal_action=petal_action, data=data)

        # Add/Remove email to the LDAP record. We need the username and email
        # address and on which mail store the Maildir lives.
        petal_action = None
        data = { 
            'username': self.login.username,
            'email': self.local + '@' + self.domain_name,
            'mail_store': self.login.mail_store.hostname
        }
        
        if desired_status == RunnerCommon.STATUS_ACTIVE:
            petal_action = RunnerCommon.PETAL_ACTION_ADD_EMAIL_ADDRESS
        else:
            # Disable and Delete are the same.
            # If any emailmailbox OTHER than ourselves exist, then we want to preserve the PostfixAccount
            # class. However, if we are the last one, then get rid of it.
            if we_are_last_mailbox:
                # We ARE the last one.
                petal_action = RunnerCommon.PETAL_ACTION_REMOVE_LAST_EMAIL_ADDRESS
            else:
                petal_action = RunnerCommon.PETAL_ACTION_REMOVE_EMAIL_ADDRESS

        petals = self.get_petals_for_category(Petal.LDAP)
        self.retire_active_runners(Petal.LDAP)
        self.create_runners(petals=petals, desired_status=desired_status, petal_action=petal_action, data=data)

    def get_absolute_url(self):
        return reverse('email-mailbox-update', args=[self.member.id, self.id])

    def __str__(self):
        return self.local + '@' + self.domain_name.__str__()

class EmailMailboxRunner(RunnerCommon):
    resource = models.ForeignKey(EmailMailbox, on_delete=models.SET_NULL, blank=True, null=True)
    petal = models.ForeignKey(Petal, on_delete=models.CASCADE)

class Access(MemberResource):
    """
    The access table lists which logins should have access to edit this member's
    records.
    """
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    login = models.ForeignKey(Login, on_delete=models.CASCADE, help_text=_("Choose from a list of your membership's logins or type in any login username available in the control panel. It does not have to be a login in your membership. The person who controls the login will have full access to your membership."))

    # Enable in django 2.2
    #class Meta:
    #    constraints = [
    #        models.UniqueConstraint(fields=['member', 'login'], name='member login')
    #    ]
    def clean(self):
        super().clean()
        exists = Access.objects.filter(member=self.member.id).filter(login=self.login.id)
        if exists and exists[0].id != self.id:
          raise ValidationError( _("You have already granted access to %(login)s.") % ({'login': self.login} ) )
            

    def get_absolute_url(self):
        return reverse('access-update', args=[self.member.id, self.id])

    def __str__(self):
        return str(self.login)

class Website(MemberResource):
    """
    The Website table holds a record for every web site.
    """
    PHP_7_2 = 1
    PHP_7_3 = 2
    
    LOGGING_DISABLED = 0
    LOGGING_ERROR_ONLY = 1
    LOGGING_ACCESS_AND_ERROR = 2

    WEBSITE_APP_NONE = 0
    WEBSITE_APP_DRUPAL = 1
    WEBSITE_APP_WORDPRESS = 2

    PHP_CHOICES = (
        (PHP_7_2, _('PHP 7.2')),
        (PHP_7_3, _('PHP 7.3')),
    )

    LOGGING_CHOICES = (
        (LOGGING_DISABLED, _('Disable logging')),
        (LOGGING_ERROR_ONLY, _('Error logging only')),
        (LOGGING_ACCESS_AND_ERROR, _('Access and error logging')),
    )

    WEBSITE_APP_CHOICES = (
        (WEBSITE_APP_NONE, _("No thanks")),
        (WEBSITE_APP_DRUPAL, _("Drupal")),
        (WEBSITE_APP_WORDPRESS, _("WordPress"))
    )

    # Define the limits allowed for max_memory and php_processes if you are not
    # a super user. These will be used in validation routines.
    USER_LIMIT_MAX_MEMORY = 512
    USER_LIMIT_PHP_PROCESSES = 12

    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    petals = models.ManyToManyField(Petal, through='WebsiteRunner')
    label = models.CharField(max_length=32, help_text=_("Required. Enter a short (32 characters), descriptive label for this site, like 'Live site' or 'Coalition site'. You can always change it later.") )
    domain_names = models.ManyToManyField(DomainName, through='WebsiteDomainName', help_text=_("Required. Select all domains that should take you to this web site. Only WEBPROXY domain names are availabe."))
    logins = models.ManyToManyField(Login, through='WebsiteLogin', help_text=_("Required. Select all the users that should be able to update your web site (SFTP access). You can also type the complete username of a login from a different membership and grant that user access."))
    tls = models.BooleanField(default=True, verbose_name=_("Encryption"), help_text=_("Turn on for https, turn off to use http only."))
    relative_document_root = models.CharField(max_length=254, blank=True, validators=[valid_path], help_text=_("Optionally, set a sub-directory of your website directory as your document root."))
    logging = models.SmallIntegerField(choices=LOGGING_CHOICES, default=LOGGING_ACCESS_AND_ERROR, help_text=_("For more privacy, you can disable logging. For trouble-shooting or to chart your site usage, you will want logging enabled."))
    php_version = models.SmallIntegerField(choices=PHP_CHOICES, default=PHP_7_3, verbose_name=_("PHP version"))
    settings = models.TextField(blank=True, validators=[valid_website_settings], help_text=_("Optionally, enter additional apache configuration settings (most sites do not need any additional settings)."))
    website_app = models.SmallIntegerField(choices=WEBSITE_APP_CHOICES, default=WEBSITE_APP_NONE, help_text=_("Optionally, automatically install a website app and allow May First to manage it's security by auto-upgrading on a daily basis. Note: If you plan to use git to manage your site, or otherwise want more control over the upgrades, do not choose this option."))
    website_app_email = models.EmailField(max_length=254, blank=True, help_text=_("Set your website app's initial admin user to this address. Required when creating a website app."))
    php_processes = models.SmallIntegerField(default=USER_LIMIT_PHP_PROCESSES, help_text=_("Limit the number of PHP processes your site may consume. The max is {0}. Set to 0 to disable PHP.".format(USER_LIMIT_PHP_PROCESSES)), verbose_name=_("PHP processes"))
    max_memory = models.PositiveIntegerField(default=256, help_text=_("PHP memory limit in Mb, contact support if you need more than {0}".format(USER_LIMIT_MAX_MEMORY)))
    tls_redirect = models.BooleanField(default=True, verbose_name=_("Automatically redirect to https?"), help_text=_("This setting is ignored on http only sites."))
    tls_custom = models.BooleanField(default=False, help_text=_("Check this box to use your own https certificate and key. Please first upload them to the tls directory and name them privkey.pem and fullchain.pem. If left unchecked, a free Let's Encrypt certificate will be automatically generated."), verbose_name=_("Custom TLS certificate and key"))
    cgi = models.BooleanField(default=False, verbose_name=_("CGI"), help_text=_("Do you plan to run CGI programs? This setting is not needed and should be off for PHP web sites. If you do need CGI, please place scripts in the cgi-bin directory not in the regular web site directory."))
    proxy_settings = models.TextField(blank=True, help_text=_("Optionally, provide additional settings for the website proxy (only accessible to admins for now)."))
    disk_quota = models.PositiveIntegerField(default=5000, help_text=_("Enter the disk quota for this web site megabytes."))
    website_store = models.ForeignKey(Petal, related_name="websites", on_delete=models.SET_NULL, blank=True, null=True, limit_choices_to={'category': Petal.WEBSTORE}, help_text=_("The canonical website host holding this web site. Set automatically to default if left blank, only change-able by admins."))
    partition = models.ForeignKey(Partition, related_name="websites", on_delete=models.SET_NULL, blank=True, null=True, limit_choices_to=models.Q(petal__category=Petal.WEBSTORE), help_text=_("The partition holding this website's data. Set automatically to default if left blank, only change-able by admins."))

    def save(self, *args, **kwargs):
        if not self.website_store:
            # Use default WEBSTORE.
            self.website_store = Petal.objects.get(is_default=True, category=Petal.WEBSTORE)

        if not self.partition:
            # Use default partition.
            self.partition = Partition.objects.get(is_default=True, petal=self.website_store)

        super().save(*args, **kwargs)

    def clean(self):
        super().clean()

        # If you are installing a website app, some fields become required.
        if self.website_app > 0 and not self.id:
            if not self.website_app_email:
                raise ValidationError( _("Please include a website app email address when installing a website app.") )

            if self.php_processes == 0:
                raise ValidationError(  _("You need at least 1 PHP process to run a website app.") )

        if not member_under_quota(self.member_id, self.disk_quota, self.id, 'Website'):
            raise ValidationError( _("You have exceeded your total membership quota. Please adjust your disk quota. Or, submit a request to increase your membership quota.") )

        if self.website_store:
            if not Petal.objects.filter(pk=self.website_store.id).exists():
                raise ValidationError( _("The website store server does not exist in the petals table.") )

    def get_runner_object(self=None):
        return WebsiteRunner

    def setup_runners(self, desired_status=None):

        # Extract list of all available PHP versions from PHP_CHOICES
        php_choices=[]
        for version in self.PHP_CHOICES:
            number=version[1].replace('PHP ','')
            #add just the version numbers to the list
            php_choices.append(number)
            # Save the version number selected by the user in another variable
            if version[0] == self.php_version :
                php_version=number


        # Create WEBSTORE runners.
        if desired_status == RunnerCommon.STATUS_ACTIVE:
            petal_action = shared.PETAL_ACTION_CREATE_WEBSTORE_SITE

            # We want a list of domain names
            domain_names = []
            for website_domain_name in self.domain_names.all():
                domain_names.append(website_domain_name.label + "." + website_domain_name.domain_zone.domain)

            # Assign a path for logs
            error_log = "/dev/null"
            access_log = "/dev/null"
            if self.logging == self.LOGGING_ERROR_ONLY:
                error_log = "/home/sites/{0}/logs/error.log".format(self.id)
            elif self.logging == self.LOGGING_ACCESS_AND_ERROR :
                access_log = "/home/sites/{0}/logs/access.log".format(self.id)
                error_log = "/home/sites/{0}/logs/error.log".format(self.id)
           

            data = {
                'server_name': domain_names.pop(0),
                'server_alias': domain_names,
                'tls': self.tls,
                'document_root': "/home/sites/{0}/web/{1}".format(self.id,self.relative_document_root).rstrip('/'),
                'access_log': access_log,
                'error_log': error_log,
                'custom_settings': self.settings,
                'php_choices': php_choices,
                'php_version': php_version,
                'php_processes': self.php_processes,
                'max_memory': self.max_memory,
                'cgi': self.cgi,
                'cgi_path': "/home/sites/{0}/cgi-bin".format(self.id),
                'partition': self.partition.get_mount_name(),
                'disk_quota': self.disk_quota,
                'id': self.id
            }

        elif desired_status == RunnerCommon.STATUS_DISABLED:
            petal_action = shared.PETAL_ACTION_DISABLE_WEBSTORE_SITE
            data = {
                'php_version': php_version,
                'id': self.id
            }
        elif desired_status == RunnerCommon.STATUS_DELETED:
            petal_action = shared.PETAL_ACTION_DELETE_WEBSTORE_SITE
            data = {
                'partition': self.partition.get_mount_name(),
                'php_version': php_version,
                'id': self.id
            }
        else:
            petal_action = shared.PETAL_ACTION_NOOP


        petals = self.get_petals_for_category(Petal.WEBSTORE)
        self.retire_active_runners(Petal.WEBSTORE)
        self.create_runners(petals=petals, petal_action=petal_action, desired_status=desired_status, data=data)


        # Create WEBPROXY runners
        if desired_status == RunnerCommon.STATUS_ACTIVE:
            petal_action = shared.PETAL_ACTION_CREATE_WEBCACHE_SITE

            data = {
                'domain_name': domain_names,
                'tls': self.tls,
                'tls_redirect': self.tls_redirect,
                'logging': self.logging,
                'proxy_settings': self.proxy_settings,
                'website_store': self.website_store.hostname,
            }

        elif desired_status == RunnerCommon.STATUS_DISABLED:
            petal_action = shared.PETAL_ACTION_DISABLE_WEBCACHE_SITE
        elif desired_status == RunnerCommon.STATUS_DELETED:
            petal_action = shared.PETAL_ACTION_DELETE_WEBCACHE_SITE
        else:
            petal_action = shared.PETAL_ACTION_NOOP


        petals = self.get_petals_for_category(Petal.WEBPROXY)
        self.retire_active_runners(Petal.WEBPROXY)
        self.create_runners(petals=petals, petal_action=shared.PETAL_ACTION_NOOP, desired_status=desired_status, data=data)

    def get_absolute_url(self):
        return reverse('website-list', args=[self.member.id])

    def __str__(self):
        return self.label

class WebsiteRunner(RunnerCommon):
    resource = models.ForeignKey(Website, on_delete=models.SET_NULL, blank=True, null=True)
    petal = models.ForeignKey(Petal, on_delete=models.CASCADE)

class WebsiteDomainName(models.Model):
    website = models.ForeignKey(Website, on_delete=models.CASCADE, help_text=_("The id of the web site."))
    domain_name = models.ForeignKey(DomainName, on_delete=models.CASCADE, limit_choices_to={'category': DomainName.WEBPROXY}, help_text=_("The id of the domain name."))

    def clean(self):
        super().clean()
        if self.website.member_id != self.domain_name.member_id:
            raise ValidationError( _("Please ensure both web site and domain name share the same membership.") )


    def __str__(self):
        return self.website + self.domain_name

class WebsiteLogin(models.Model):
    website = models.ForeignKey(Website, on_delete=models.CASCADE, help_text=_("The id of the website record."))
    login = models.ForeignKey(Login, on_delete=models.CASCADE, limit_choices_to={'shell_access': True}, help_text=_("The id of the login record."))

    # Note: It is allowed to grant a login from a different membership access to your web site. 

    def __str__(self):
        return self.website + self.login

class EmailAlias(MemberResource):
    """
    The EmailAlias table holds records for all email addresses that forward
    to one or more email addresses.
    """
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    petals = models.ManyToManyField(Petal, through='EmailAliasRunner')
    local = models.CharField(max_length=254, validators=[valid_user_portion], help_text=_("The portion of the email address before the @ sign."))
    domain_name = models.CharField(max_length=254, validators=[valid_domain_name], help_text=_("The portion of the email address after the @ sign."))
    target = models.TextField(verbose_name=_("Target addresses"), validators=[valid_space_separated_email_addresses], help_text=_("Enter up to 10 space separated email addresses that should receive all email sent to the alias address."))

    def clean(self):
        super().clean()
        # Don't let someone register an email using another persons domain zone.
        psl = PublicSuffixList()
        private_suffix = psl.privatesuffix(self.domain_name)
        result = DomainZone.objects.filter(domain__endswith=private_suffix).exclude(member_id=self.member_id).exists()
        if result:
            raise ValidationError( _("That domain is in use by another member.") )

    def get_absolute_url(self):
        return reverse('email-alias-update', args=[self.member.id, self.id])

    def get_type(self):
        return 'alias'

    def get_display_type(self):
        return _("Alias")

    def get_display_destination(self):
        return self.target

    def get_runner_object(self=None):
        return EmailAliasRunner

    def setup_runners(self, desired_status=None):
        if desired_status == RunnerCommon.STATUS_ACTIVE:
            petal_action = RunnerCommon.PETAL_ACTION_ADD_EMAIL_ALIAS
        else:
            petal_action = RunnerCommon.PETAL_ACTION_REMOVE_EMAIL_ALIAS

        targets = []
        for email in self.target.split(' '):
            targets.append(email)

        data = {
            'email': self.local + '@' + self.domain_name,
            'targets': targets
        }

        petals = self.get_petals_for_category(Petal.LDAP)
        self.retire_active_runners(Petal.LDAP)
        self.create_runners(petals=petals, petal_action=petal_action, desired_status=desired_status, data=data)

    def __str__(self):
        return self.local + "@" + self.domain_name

class EmailAliasRunner(RunnerCommon):
    resource = models.ForeignKey(EmailAlias, on_delete=models.SET_NULL, blank=True, null=True)
    petal = models.ForeignKey(Petal, on_delete=models.CASCADE)

class MysqlUser(MemberResource):
    READWRITE = 1
    READONLY = 2

    PERMISSION_CHOICES = (
        (READWRITE, _("Read and write access")),
        (READONLY, _("Read only access")),
    )
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    mysql_database = models.ForeignKey('MysqlDatabase', on_delete=models.CASCADE)
    user = models.CharField(unique=True, max_length=16, validators=[valid_mysql_user])
    password = models.CharField(max_length=254, validators=[valid_password], help_text=_("Database passwords are randomly chosen for you and cannot be entered manually. A password is only required when creating a new record."))
    permission = models.SmallIntegerField(choices=PERMISSION_CHOICES, default=READWRITE, help_text=_("Either 1 for read and write access or 2 for read only access."))

    def save(self, *args, **kwargs):
        """
        Convert password to mysql encoded password before saving.
        """
        if self.password:
            # A mysql hashed password is 41 characters long and starts with an asterisk.
            # We don't let users choose their own passwords (and our generated ones are 25
            # characters long). So we can safely assume that a 41 character password that
            # starts with an asterisk is a converted one and we don't need to convert it
            # again.
            if len(self.password) == 41 and self.password.startswith('*'):
                pass
            else:
                self.password = utils.convert_to_mysql_password_hash(self.password)

        super().save(*args, **kwargs)

    def clean(self):
        super().clean()
        if self.mysql_database.member_id != self.member_id:
            raise ValidationError( _("That mysql database belongs to another member.") )

    def get_absolute_url(self):
        return reverse('mysql-user-update', args=[self.member.id, self.id])

    def __str__(self):
        return self.user

class MysqlDatabase(MemberResource):
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    petals = models.ManyToManyField(Petal, through='MysqlDatabaseRunner')
    name = models.CharField(unique=True, max_length=64, validators=[valid_mysql_database])

    def get_absolute_url(self):
        return reverse('mysql-database-list', args=[self.member.id])

    def __str__(self):
        return self.name

class MysqlDatabaseRunner(RunnerCommon):
    resource = models.ForeignKey(MysqlDatabase, on_delete=models.SET_NULL, blank=True, null=True)
    petal = models.ForeignKey(Petal, on_delete=models.CASCADE)
