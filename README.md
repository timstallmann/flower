# Flower: The May First control panel

This repo contains two django apps that power the [May First Movement
Technology](https://mayfirst.coop) control panel, allowing May First members to
create user accounts, email addresses, mailing lists, domain names, web sites
and more.

The code name for the entire project is "flower."

## Hacking

Want to help with development?

Install Debian buster/bullseye dependencies:

`apt install python3-pip python3-publicsuffix python3-wheel python3-sqlparse default-libmysqlclient-dev python3-dev python3-tabulate`

(In buster, you will need python3-publicsuffix, in bullseye, python3-publicsuffix2.)

(We need `default-libmysqlclient-dev` and `python3-dev` so we can pull in
mysqlclient from pip3 - since django requires a version of mysql that is more
recent that what is provided via Debian. Those are not needed if you are
running in dev mode with a sqlite database.)

Then, we need to pull in our dependencies locally.

First, the hub dependencies:

`pip3 install --user -r requirements.txt` 

And, next the petal dependencies:

`pip3 install --user -r flowerpetal/requirements.txt`

If you are running a production instance or one that requires mysql:

`pip3 install --user mysqlclient`

### Which part to run?

Flower comes with two apps:

 * *The Hub:* This app is the user interface. It provides a web server for users
   to create, edit and delete resources. This is the default app.

 * *The Petal:* This app runs on ever server on which resources are created.
   Every time a resource is created in the hub, a POST is made to the petal
   version of the app so it can be created. To run the petal app, set the
   environment variable `FLOWER_APP` to `petal`.

### What mode to run?

Flower can be run in one of three modes:

 * dev: The default. In the petal app, `sudo` commands that would normally try
   to create resourcs on the server are instead echoed to standard out. This is
   the default.

 * production: all systems operate as if we are running in real life. To run in
   production mode, set `FLOWER_MODE` to `production`.

 * test: Like production, but private IPs are allowed. To run in test mode, set
   `FLOWER_MODE` to `test`.

### What about the demo?

The hub app will behave differently if it is run with the `FLOWER_DEMO`
environment variable set to `1`. It will display a home page allowing anyone to
create a membership and it will not attempt to post anything to a petal server,
instead simply making each resource automatically `active`.

*Tip: If you are only interested in front end development, running the hub with
`FLOWER_DEMO=1` is a great idea because then you don't have to worry about
running the petal app at all.*

### How to launch

Most of the steps below should be run from the root directory of the project
(the level that has the manage.py file in it).

#### Running the hub app in dev mode

`dev` mode and the `hub` hub app are the defaults so you don't have to specify
them via environment variables.

First, you have to create your database and tables. By default, you will use
sqlite, so no need to setup anything in advance, simply run these commands:

    python3 manage.py makemigrations
    python3 manage.py migrate

The control panel must be configured with the address of default servers and
partitions on which to install resources. You can pre-populate some default
servers and partitions with:

    python3 manage.py prepopulate

Next, create the super user:

    python3 manage.py createsuperuser

Launch the hub app (will listen on http://localhost:8000):

    python3 manage.py runserver

And, in another terminal window, launch the petal app (will listen on
http://localhost:8888):

    FLOWER_APP=petal python3 manage.py runserver 8888

Or... just launch the app in demo mode and don't launch the hub:

    FLOWER_DEMO=1 python3 manage.py runserver

Now, point your web browser to: https://localhost:8000/ and login with the
username and password you create during the superuser step above.

### Start over

By default, in dev mode a sqlite database is used that is left in the root
directory and called db.hub.sqlite3.

You can always delete that file and run `rm flowerhub/migrations/0*.py` and
repeat all the steps above.

## In production

In dev mode, django manages to serve static files on the fly.

In production, we can instruct django to collect all the static resources once so it's more efficient:

    python3 manage.py collectstatic

#### Add to apache

It's advisable to put apache or nginx in front of your django apps.

Here's a configuration that works with apache as a front end:

    Alias /static /home/members/mayfirst/sites/cp.mayfirst.org/include/flower/static-root
    RequestHeader set X-Forwarded-Proto 'https' env=HTTPS
    ProxyPass /static !
    ProxyPass / http://localhost:8000/

Here's what works with nginx:

    upstream django {
        server 127.0.0.1:8000 fail_timeout=0; 
    }

    # configuration of the server
    server {
        # path for static files
        root /home/jamie/projects/mfpl/flower/static-root;

        location / {
          # checks for static file, if not found proxy to app
          try_files $uri @proxy_to_app;
        }

        location @proxy_to_app {
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_set_header X-Forwarded-Proto $scheme;
          proxy_set_header Host $http_host;
          proxy_redirect off;
          proxy_pass http://django;
        }

        error_page 500 502 503 504 /500.html;
        location = /500.html {
          root /home/jamie/projects/mfpl/flower/petal/static;
        }
    }

### Use gunicorn

When running in production, we want a more robust web server then the one
built-in to django. So, we launch the app with gunicorn.

    FLOWER_MODE=production gunicorn3 flower.wsgi:application

Or... in demo mode:

    FLOWER_MODE=production FLOWER_DEMO=1 gunicorn3 flower.wsgi:application

### Running the petal 

Each petal has to be launched as well (note the different port):

    FLOWER_APP=petal gunicorn3 -b 127.0.0.1:8888 flower.wsgi:application

Since the petal servers are only accessed via the hub, a front end is not that important.

## Common tasks

Whenever new translation strings are added, run: `python3 manage.py compilemessages`

Whenever you want to clear everything in the demo site (Carefull! Deletes all members!!): `FLOWER_MODE=proudction FLOWER_DEMO=1 python3 manage.py cleardemo`

## Random notes on locking

We don't do any locking, but do take some precautions to ensure we don't have problems.

There are three distinct concerns:

1. User Interface conflicts: One user opens a login record for editing and
   changes the first name. A second user opens the same login record for
   editing and changes the first name. Both users hit submit and both users
   think their first name is saved, however, the last person to hit submit
   actually wins. 
   
   There is no mechanism in place to stop this behavior because:

   a. It's not a serious problem: unlike publishing systems where people spend
   a long time writing something only to have it overwritten by someone else,
   everything in the control panel can be re-done in a matter of minutes.

   b. Users can see what happened easily in the history tab and will understand
   and accept it.

   c. Fixing it means adding a locking record and cache time which adds
   complexity to the code and (since you can't view a record without clicking
   the edit button) will lead to a lot of people getting a record conflict
   warning that is spurious and confusing.

2. Data out of sync problems: Two people hit submit on the same record at
   exactly the same time. One record wins in the database but it's possible
   that the other record will win when applied on the server. Although this is
   highly unusual, if it happens it will be really confusing and difficult to
   debug and make the control panel seem unpredictable.

   Solution: If you try to save a record that is in a pending state, the save is
   rejected. A record must be active, disabled or non-existent to make a change.

3. Data corruption on server: Two records are saved to a petal at the same time
   resulting in a file being written to by two processes at the same time.

   Solution: this shouldn't happen due to number 2 above... but as a precaution
   all petal data writes should be atomic (e.g. made via an ldap client which
   should enure there is no data corruption or when writing files, write to a
   temp file on the target file system and then put in place with a `mv`
   command).
