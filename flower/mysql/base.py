from django.db.backends.mysql import base, schema

# See: https://bd808.com/blog/2017/04/17/making-django-migrations-that-work-with-mysql-55-and-utf8mb4/
# and https://github.com/wikimedia/debmonitor/commit/6219784ec0187a5f4465dd368931bfb8c77e2f50
# After Buster, this can probably be dropped since ROW_FORMAT=DYNAMIC will be the default
# with mariadb starting with 10.2.2 (https://mariadb.com/kb/en/library/innodb-storage-formats/)

class DatabaseSchemaEditor(schema.DatabaseSchemaEditor):
    """Override the default MySQL database schema editor to add ROW_FORMAT=dynamic."""
    sql_create_table = "CREATE TABLE %(table)s (%(definition)s) ROW_FORMAT=DYNAMIC"


class DatabaseWrapper(base.DatabaseWrapper):
    """Override the default MySQL database wrapper to use the custom schema editor class."""
    SchemaEditorClass = DatabaseSchemaEditor
