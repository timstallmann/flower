"""
Django settings for flower project.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.1/ref/settings/

This settings file is dynamically adjusted depending on the environment
variables set when it is invoked. There are two environment variables and
many combinations that result.

FLOWER_APP: either hub or petal. Determines which app we are launching as. The hub 
is a full django web-based front end for users. The petal, on the other hand, is a
minimal api only interface for creating resources.

FLOWER_MODE: production, test, or dev. Hopefully obvious. 

FLOWER_DEMO: 1 or 0 or empty. With demo turned on no attempt is made to connect
to a petal to have the resources created.  It simply marks all resources as
'active' as soon as they are created. Also, the home page allows people to
create their own memberships.

Because there are three variables, a lot of combinations are possible, e.g.:

    hub-dev-demo
    hub-test
    petal-dev
    petal-produciton-demo
    etc.

""" 

import os
from django.utils.translation import gettext_lazy as _

# First define a few django settings that apply regardless of the app or mode. 

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
ROOT_URLCONF = 'flower.urls'
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'flower.wsgi.application'
TIME_ZONE = 'America/New_York'
USE_TZ = True


# Define possible FLOWER modes: 
FLOWER_MODE_PRODUCTION = 'production'
FLOWER_MODE_TEST = 'test'
FLOWER_MODE_DEV = 'dev'

# Define possible apps and their meaning
FLOWER_APP_HUB = 'hub'
FLOWER_APP_PETAL = 'petal'

# Default mode is dev mode so we don't have to type as much!
if os.environ.get('FLOWER_MODE'):
    if os.environ.get('FLOWER_MODE') == 'production':
        FLOWER_MODE = FLOWER_MODE_PRODUCTION
    elif os.environ.get('FLOWER_MODE') == 'test':
        FLOWER_MODE = FLOWER_MODE_TEST
    elif os.environ.get('FLOWER_MODE') == 'dev':
        FLOWER_MODE = FLOWER_MODE_DEV

    else:
        raise ValueError("I don't unserstand the FLOWER_MODE you passed in ({0}). Please use production, test, dev or demo.". format(os.environ.get('FLOWER_MODE')))
else:
    FLOWER_MODE = FLOWER_MODE_DEV

if os.environ.get('FLOWER_DEMO') == '1':
    FLOWER_DEMO = True
else:
    FLOWER_DEMO = False

# Default app is hub.
if os.environ.get('FLOWER_APP'):
    if os.environ.get('FLOWER_APP') == 'hub':
        FLOWER_APP = FLOWER_APP_HUB
    elif os.environ.get('FLOWER_APP') == 'petal':
        FLOWER_APP = FLOWER_APP_PETAL
    else:
        raise ValueError("I don't unserstand the FLOWER_APP you passed in ({0}). Please use hub or petal.". format(os.environ.get('FLOWER_APP')))
else:
    FLOWER_APP = FLOWER_APP_HUB

# Override default variables depending on what mode and app we are in. 

# Changes based on APP. The following apply to the hub regardless of the mode.
if FLOWER_APP == FLOWER_APP_HUB:
    LANGUAGE_CODE = 'en'
    LANGUAGES = [
        ('en', _('English')),
        ('es', _('Spanish')),
    ]
    USE_I18N = True
    USE_L10N = True
    REST_FRAMEWORK = {
        # Use Django's standard `django.contrib.auth` permissions,
        # or allow read-only access for unauthenticated users.
        'DEFAULT_PERMISSION_CLASSES': [
            'rest_framework.permissions.IsAuthenticated'
        ]
    }
    MIDDLEWARE = [
        'django.middleware.security.SecurityMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.locale.LocaleMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
        'flowerhub.middleware.CurrentUserMiddleware',
    ]

    INSTALLED_APPS = [
        'django.contrib.auth',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'django.contrib.contenttypes',
        'rest_framework',
        'flowerhub.apps.FlowerhubConfig'
    ]

    STATIC_URL = '/static/'
    STATIC_ROOT = BASE_DIR + '/static-root'
    LOGIN_REDIRECT_URL = 'member-list'

    FLOWER_HOST_MEMBER_ID = 1

    # Define domains designed for internal IP addresses.
    FLOWER_PRIVATE_DOMAINS = [ "mayfirst.cx" ]
    FLOWER_PRIVATE_NETWORKS = [ "10.9.67.0/24" ]

    FLOWER_PASSWORD_MINIMUM_LENGTH = 10

    FLOWER_PASSWORD_RESET_VALID_DAYS = 7

    USE_X_FORWARDED_HOST = True
    SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

    FLOWER_PETAL_SUBMIT = True

elif FLOWER_APP == FLOWER_APP_PETAL:
    # The following apply to the the petal regardless of the mode.
    MIDDLEWARE = []

    # Flower
    INSTALLED_APPS = [
        'django.contrib.contenttypes',
        'flowerpetal.apps.FlowerpetalConfig',
    ]

    # On the hub we use jinja2 templates so we can use the -%} syntax
    # to avoid line breaks in our configuration files.
    TEMPLATES = [
        {
            'BACKEND': 'django.template.backends.jinja2.Jinja2',
            'APP_DIRS': True,
            'OPTIONS': { },
        }
    ]

# Now make changes based on the mode.
if FLOWER_MODE == FLOWER_MODE_PRODUCTION:
    DEBUG = False 

    if FLOWER_APP == FLOWER_APP_HUB:
        if os.path.exists('flower/settings_local_production_hub.py'):
            from flower.settings_local_production_hub import *
    elif FLOWER_APP == FLOWER_APP_PETAL:
        if os.path.exists('flower/settings_local_production_petal.py'):
            from flower.settings_local_production_petal import *

elif FLOWER_MODE == FLOWER_MODE_TEST:
    DEBUG = False 

    if FLOWER_APP == FLOWER_APP_HUB:
        if os.path.exists('flower/settings_local_test_hub.py'):
            from flower.settings_local_test_hub import *
    elif FLOWER_APP == FLOWER_APP_PETAL:
        if os.path.exists('flower/settings_local_test_petal.py'):
            from flower.settings_local_test_petal import *

elif FLOWER_MODE == FLOWER_MODE_DEV:
    # To ease development, we don't need to pull in any special local settings.
    # Instead, we provide settings that should work out of the box for
    # everyone.
    DEBUG = True
    SECRET_KEY = '7$l#9y$ayubz17@m0=zq6&61l4inejze*$hwam2shf&vo&x%32'
    FLOWER_BASE_URL = 'http://127.0.0.1:8000/'

    if FLOWER_APP == FLOWER_APP_HUB:
        DATABASES = {
            'default': {
                'ENGINE': 'django.db.backends.sqlite3',
                'NAME': os.path.join(BASE_DIR, 'db.hub.sqlite3'),
            }
        }

        FLOWER_ALLOW_PRIVATE_IPS = True
        ALLOWED_HOSTS = [ '127.0.0.1', 'localhost' ]
        USE_X_FORWARDED_HOST = False 
        SECURE_PROXY_SSL_HEADER = None 
        FLOWER_PETAL_SUBMIT = True 
        INTERNAL_IPS = [ "127.0.0.1" ]
        # These domains get to skip validation if being added by the host organization.
        FLOWER_HOST_ALLOWED_PUBLIC_SUFFIXES = [ "mayirst.org", "mayfirst.info", "in-addr.arpa" ]

        # PTR networks are IP ranges that we can set pointer records for.
        FLOWER_IPV6_PTR_NETWORKS = [ "2001:470:1:116::/64" ]
        FLOWER_IPV4_PTR_NETWORKS = [ "1.2.3.0/24" ]

        # MX Domains are set whenever you create a zone.
        FLOWER_MX_DOMAINS = [ "a.mx.mayfirst.dev", "b.mx.mayfirst.dev" ]

        FLOWER_NAME_SERVERS = [ "a.ns.mayfirst.org", "b.ns.mayfirst.org" ]

        # Webproxy records are resolved and converted into A records prior
        # to insertion into the DNS server. They are not designed to be
        # aliasess or flattened CNAMES (i.e. resolved by the authoritative
        # server when requested). Therefore, the server name should be
        # restricted to domain names controlled by the host.
        FLOWER_WEBPROXY_DEFAULT_SERVER_NAME = "webproxy.mayfirst.dev"
        FLOWER_WEBPROXY_SERVER_NAME_REGEXP = '^webproxy(-[a-z0-9]+)?\.mayfirst\.dev$' 

        FLOWER_PETAL_API_KEY = 'flower'

        if os.path.exists('flower/settings_local_dev_hub.py'):
            from flower.settings_local_dev_hub import *

    elif FLOWER_APP == FLOWER_APP_PETAL:
        # This is the hashed version of the password is "flower"
        FLOWER_PETAL_API_KEY_HASH = 'c06b0cfe0cc5e900c57784484094331f095bf441995c3c31ea6c75691c786c35'

    
        if os.path.exists('flower/settings_local_dev_petal.py'):
            from flower.settings_local_dev_petal import *


